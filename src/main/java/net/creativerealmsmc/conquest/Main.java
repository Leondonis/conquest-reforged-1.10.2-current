package net.creativerealmsmc.conquest;

import net.creativerealmsmc.conquest.proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = Main.MODID, name = Main.MODNAME)
public class Main {

	public static final String MODID = "conquest";
	public static final String MODNAME = "Conquest Mod";

	@Instance(Main.MODID)
	public static Main instance;

	@SidedProxy(clientSide="net.creativerealmsmc.conquest.proxy.ClientProxy", serverSide="net.creativerealmsmc.conquest.proxy.ServerProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit(e);
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent e) {
		proxy.serverStart(e);
	}
}