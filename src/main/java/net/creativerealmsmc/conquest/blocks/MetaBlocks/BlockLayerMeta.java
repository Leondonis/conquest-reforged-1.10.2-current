package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 12/5/2016.
 */

public class BlockLayerMeta extends Block implements IMetaBlockName
{
    public static final PropertyEnum<BlockLayerMeta.EnumType> VARIANT = PropertyEnum.<BlockLayerMeta.EnumType>create("variant", BlockLayerMeta.EnumType.class);
    public static final PropertyEnum<BlockLayerMeta.EnumHeight> HEIGHT = PropertyEnum.<BlockLayerMeta.EnumHeight>create("height", BlockLayerMeta.EnumHeight.class);
    protected static final AxisAlignedBB[] SNOW_AABB = new AxisAlignedBB[]{
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.625D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D), 
    		new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};

    public BlockLayerMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, CreativeTabs tab)
    {
        super(materialIn);
        this.setDefaultState(this.blockState.getBaseState().withProperty(HEIGHT, EnumHeight.HEIGHT2).withProperty(VARIANT, EnumType.ALPHA)); // is never 'double'
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        this.useNeighborBrightness = true;
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
    }
    
    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }
    
    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, World worldIn, BlockPos pos)
    {
        int i = ((EnumHeight)blockState.getValue(HEIGHT)).getMetadata();
        float f = 0.125F;
        AxisAlignedBB axisalignedbb = blockState.getBoundingBox(worldIn, pos);
        return new AxisAlignedBB(axisalignedbb.minX, axisalignedbb.minY, axisalignedbb.minZ, axisalignedbb.maxX, axisalignedbb.maxY, axisalignedbb.maxZ);
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return SNOW_AABB[((EnumHeight)state.getValue(HEIGHT)).getMetadata()];
    }

    public boolean isPassable(IBlockAccess worldIn, BlockPos pos)
    {
        return ((EnumHeight)worldIn.getBlockState(pos).getValue(HEIGHT)).getMetadata() < 5;
    }
    
    public boolean isFullyOpaque(IBlockState state)
    {
        return ((EnumHeight)state.getValue(HEIGHT)).getMetadata() == 7;
    }
    
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
    
    public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos)
    {
        return ((EnumHeight)worldIn.getBlockState(pos).getValue(HEIGHT)).getMetadata() == 0;
    }
    
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        if (side == EnumFacing.UP)
        {
            return true;
        }
        else
        {
            IBlockState iblockstate = blockAccess.getBlockState(pos.offset(side));
            return iblockstate.getBlock() == this && ((EnumHeight)iblockstate.getValue(HEIGHT)).getMetadata() >= ((EnumHeight)blockState.getValue(HEIGHT)).getMetadata() ? true : super.shouldSideBeRendered(blockState, blockAccess, pos, side);
        }
    }
    
    @Override
    public int damageDropped(IBlockState state)
    {
    	EnumType enumvariant = (EnumType)state.getValue(VARIANT);
    	return enumvariant.getMetadata();
    }

    
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list)
    {
        for (EnumType blocklayerr$enumtype : EnumType.values())
        {
            list.add(new ItemStack(itemIn, 1, blocklayerr$enumtype.getMetadata()));
        }
    }

    public IBlockState getStateFromMeta(int meta)
    {
    	int variantbits = (meta & 0x01);
    	int heightbits = (meta & 0x0E) >> 1;
        
    	EnumType variant = EnumType.byMetadata(variantbits);
        EnumHeight height = EnumHeight.byMetadata(heightbits);
        
        return this.getDefaultState().withProperty(VARIANT, variant).withProperty(HEIGHT, height);
    }

    public int getMetaFromState(IBlockState state)
    {
    	EnumType variant = (EnumType)state.getValue(VARIANT);
    	EnumHeight height = (EnumHeight)state.getValue(HEIGHT);
    	
    	int variantbits = variant.getMetadata();
        int heightbits = height.getMetadata() << 1;
        
        return heightbits | variantbits;
    }
    
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {HEIGHT, VARIANT});
    }
    
    public static enum EnumHeight implements IStringSerializable
    {
    	HEIGHT2(0,"height2"),
    	HEIGHT4(1,"height4"),
    	HEIGHT6(2,"height6"),
    	HEIGHT8(3,"height8"),
    	HEIGHT10(4,"height10"),
    	HEIGHT12(5,"height12"),
    	HEIGHT14(6, "height14"),
    	FULL(7,"full");
    	
    	private static final EnumHeight[] META_LOOKUP = new EnumHeight[values().length];
        private final int meta;
        private final String name;
        
        private EnumHeight(int metaIn, String nameIn)
        {
        	this.meta = metaIn;
        	this.name = nameIn;
        }

        public int getMetadata()
        {
        	return this.meta;
        }

        @Override
        public String toString()
        {
        	return this.name;
        }
        
        public static EnumHeight byMetadata(int meta)
        {
        	if (meta < 0 || meta > META_LOOKUP.length)
        	{
        		meta = 0;
        	}
        	return META_LOOKUP[meta];
        }
        
        public String getName()
        {
        	return this.name;
        }
        
        static
        {
        	for (EnumHeight blocklayer$enumheight : values())
        	{
        		META_LOOKUP[blocklayer$enumheight.getMetadata()] = blocklayer$enumheight;
        	}
        }
    }
    
    public static enum EnumType implements IStringSerializable
    {
        ALPHA(0, "alpha"),
        BRAVO(1, "bravo");
    	
    	private static final EnumType[] META_LOOKUP = new EnumType[values().length];
        private final int meta;
        private final String name;
        
        private EnumType(int metaIn, String nameIn)
        {
        	this.meta = metaIn;
        	this.name = nameIn;
        }

        public int getMetadata()
        {
        	return this.meta;
        }

        @Override
        public String toString()
        {
        	return this.name;
        }
        
        public static EnumType byMetadata(int meta)
        {
        	if (meta < 0 || meta > META_LOOKUP.length)
        	{
        		meta = 0;
        	}
        	return META_LOOKUP[meta];
        }
        
        public String getName()
        {
        	return this.name;
        }
        
        static
        {
        	for (EnumType blocklayer$enumtype : values())
        	{
        		META_LOOKUP[blocklayer$enumtype.getMetadata()] = blocklayer$enumtype;
        	}
        }
    }
}