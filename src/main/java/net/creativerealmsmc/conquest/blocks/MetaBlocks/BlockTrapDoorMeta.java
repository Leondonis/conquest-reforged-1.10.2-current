package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 12/5/2016.
 */

public class BlockTrapDoorMeta extends BlockDirectional implements IMetaBlockName
{
	public static final PropertyEnum<BlockTrapDoorMeta.EnumType> VARIANT = PropertyEnum.<BlockTrapDoorMeta.EnumType>create("variant", BlockTrapDoorMeta.EnumType.class);
    protected static final AxisAlignedBB AABB_NORTH = new AxisAlignedBB(0.0D, 0.0D, 0.8125D, 1.0D, 1.0D, 1.0D);
    protected static final AxisAlignedBB AABB_SOUTH = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.1875D);
    protected static final AxisAlignedBB AABB_WEST = new AxisAlignedBB(0.8125D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
    protected static final AxisAlignedBB AABB_EAST = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.1875D, 1.0D, 1.0D);
    protected static final AxisAlignedBB AABB_TOP = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.1875D, 1.0D);
    protected static final AxisAlignedBB AABB_BOTTOM = new AxisAlignedBB(0.0D, 0.8125D, 0.0D, 1.0D, 1.0D, 1.0D);
    private boolean canSustainPlant;

    public BlockTrapDoorMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
    	super(materialIn);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.UP).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }
    
    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }
    
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
    
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }
    
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        AxisAlignedBB axisalignedbb;

        {
            switch ((EnumFacing)state.getValue(FACING))
            {
                case NORTH:
                default:
                    axisalignedbb = AABB_NORTH;
                    break;
                case SOUTH:
                    axisalignedbb = AABB_SOUTH;
                    break;
                case WEST:
                    axisalignedbb = AABB_WEST;
                    break;
                case EAST:
                    axisalignedbb = AABB_EAST;
                    break;
                case UP:
                    axisalignedbb = AABB_TOP;
                    break;
                case DOWN:
                    axisalignedbb = AABB_BOTTOM;
            }

        return axisalignedbb;
        }
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
    {
    	EnumType[] allType = EnumType.values();
    	for (EnumType type : allType) 
    	{
    		list.add(new ItemStack(itemIn, 1, type.getMetadata()));
    	}
    }
    
    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
    	IBlockState iblockstate = worldIn.getBlockState(pos.offset(facing.getOpposite()));
    	return this.getDefaultState().withProperty(FACING, facing).withProperty(VARIANT, EnumType.byMetadata(meta));
    }
    
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return true;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        int facingbits = (meta & 14) >> 1;
        EnumFacing facing = EnumFacing.getFront(facingbits);
        EnumType variant = EnumType.byMetadata(meta & 1);
        return this.getDefaultState().withProperty(VARIANT, variant).withProperty(FACING, facing);
    }

    public int getMetaFromState(IBlockState state)
    {
    	EnumFacing facing = (EnumFacing)state.getValue(FACING);
    	EnumType variant = (EnumType)state.getValue(VARIANT);

        int facingbits = facing.getIndex() << 1;
        int variantbits = variant.getMetadata();
        return facingbits | variantbits;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        return state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }
    
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn)
    {
        return state.withProperty(FACING, mirrorIn.mirror((EnumFacing)state.getValue(FACING)));
    }
    
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {FACING, VARIANT});
    }

    public int damageDropped(IBlockState state)
    {
        return ((BlockTrapDoorMeta.EnumType)state.getValue(VARIANT)).getMetadata();
    }
    
    public static enum EnumType implements IStringSerializable
    {
    	ALPHA(0, "alpha"),
    	BRAVO(1, "bravo");

        private static final BlockTrapDoorMeta.EnumType[] META_LOOKUP = new BlockTrapDoorMeta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockTrapDoorMeta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockTrapDoorMeta.EnumType blockhorizontal$enumtype : values())
            {
                META_LOOKUP[blockhorizontal$enumtype.getMetadata()] = blockhorizontal$enumtype;
            }
        }
    }
}
