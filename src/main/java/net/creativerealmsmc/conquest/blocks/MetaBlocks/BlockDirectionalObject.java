package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import com.google.common.collect.Lists;
import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.BlockDirectional;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.obj.OBJModel;
import net.minecraftforge.common.model.TRSRTransformation;
import net.minecraftforge.common.property.ExtendedBlockState;
import net.minecraftforge.common.property.IExtendedBlockState;
import net.minecraftforge.common.property.IUnlistedProperty;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 12/28/2016.
 */

public class BlockDirectionalObject extends BlockDirectional implements IMetaBlockName
{
	public ExtendedBlockState state = new ExtendedBlockState(this, new IProperty[]{FACING}, new IUnlistedProperty[]{OBJModel.OBJProperty.INSTANCE});
	public static final PropertyEnum<EnumType> VARIANT = PropertyEnum.<EnumType>create("variant", EnumType.class);

    public BlockDirectionalObject(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, String tool, int toolLevel)
    {
    	super(materialIn);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.UP).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(CreativeTabs.SEARCH);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setHarvestLevel(tool, toolLevel);
    }
    
    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return true;
    }
    
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
    
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }
    
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
    
    public boolean isFullyOpaque(IBlockState state)
    {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
    {
    	EnumType[] allType = EnumType.values();
    	for (EnumType type : allType) 
    	{
    		list.add(new ItemStack(itemIn, 1, type.getMetadata()));
    	}
    }
    
    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
    	IBlockState iblockstate = worldIn.getBlockState(pos.offset(facing.getOpposite()));
    	return this.getDefaultState().withProperty(FACING, facing).withProperty(VARIANT, EnumType.byMetadata(meta));
    }
    
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return true;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        int facingbits = (meta & 14) >> 1;
        EnumFacing facing = EnumFacing.getFront(facingbits);
        EnumType variant = EnumType.byMetadata(meta & 1);
        return this.getDefaultState().withProperty(VARIANT, variant).withProperty(FACING, facing);
    }

    public int getMetaFromState(IBlockState state)
    {
    	EnumFacing facing = (EnumFacing)state.getValue(FACING);
    	EnumType variant = (EnumType)state.getValue(VARIANT);

        int facingbits = facing.getIndex() << 1;
        int variantbits = variant.getMetadata();
        return facingbits | variantbits;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        return state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }
    
    public IBlockState withMirror(IBlockState state, Mirror mirrorIn)
    {
        return state.withProperty(FACING, mirrorIn.mirror((EnumFacing)state.getValue(FACING)));
    }
    
    protected BlockStateContainer createBlockState()
    {
        return new ExtendedBlockState(this, new IProperty[] {FACING, VARIANT}, new IUnlistedProperty[]{OBJModel.OBJProperty.INSTANCE});
    }

    public int damageDropped(IBlockState state)
    {
        return ((EnumType)state.getValue(VARIANT)).getMetadata();
    }
    
    @Override
    public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos)
    {
    	EnumFacing facing = (EnumFacing) state.getValue(FACING);
        TRSRTransformation transform = new TRSRTransformation(facing);
        OBJModel.OBJState newState = new OBJModel.OBJState(Lists.newArrayList(OBJModel.Group.ALL), true, transform);
        return ((IExtendedBlockState) state).withProperty(OBJModel.OBJProperty.INSTANCE, newState);
    }
    
    public static EnumFacing getFacingFromEntity(World worldIn, BlockPos clickedBlock, EntityLivingBase entityIn)
    {
        if (MathHelper.abs((float)entityIn.posX - (float)clickedBlock.getX()) < 2.0F && MathHelper.abs((float)entityIn.posZ - (float)clickedBlock.getZ()) < 2.0F)
        {
            double d0 = entityIn.posY + (double)entityIn.getEyeHeight();
        }

        return entityIn.getHorizontalFacing().getOpposite();
    }
    
    public static enum EnumType implements IStringSerializable
    {
        ALPHA(0, "alpha"),
        BRAVO(1, "bravo");
    	
    	private static final EnumType[] META_LOOKUP = new EnumType[values().length];
        private final int meta;
        private final String name;
        
        private EnumType(int metaIn, String nameIn)
        {
        	this.meta = metaIn;
        	this.name = nameIn;
        }

        public int getMetadata()
        {
        	return this.meta;
        }

        @Override
        public String toString()
        {
        	return this.name;
        }
        
        public static EnumType byMetadata(int meta)
        {
        	if (meta < 0 || meta > META_LOOKUP.length)
        	{
        		meta = 0;
        	}
        	return META_LOOKUP[meta];
        }
        
        public String getName()
        {
        	return this.name;
        }
        
        static
        {
        	for (EnumType enumtype : values())
        	{
        		META_LOOKUP[enumtype.getMetadata()] = enumtype;
        	}
        }
    }
}
