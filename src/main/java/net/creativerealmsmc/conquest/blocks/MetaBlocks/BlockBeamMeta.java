package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import java.util.List;
import java.util.Locale;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Rotation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

/**
 * Created by Artemisia on 12/28/2016.
 */

public class BlockBeamMeta extends BlockHorizontal implements IMetaBlockName
{
	public static final PropertyBool ACTIVATED = PropertyBool.create("activated");
	public static final PropertyEnum<EnumType> VARIANT = PropertyEnum.<EnumType>create("variant", EnumType.class);
    protected static final AxisAlignedBB AABB_NORTH_OFF = new AxisAlignedBB(0.0D, 0.0D, 0.3125D, 0.25D, 1.0D, 0.6875D);
    protected static final AxisAlignedBB AABB_SOUTH_OFF = new AxisAlignedBB(0.75D, 0.0D, 0.3125D, 1.0D, 1.0D, 0.6875D);
    protected static final AxisAlignedBB AABB_WEST_OFF = new AxisAlignedBB(0.3125, 0.0D, 0.75D, 0.6875D, 1.0D, 1.0D);
    protected static final AxisAlignedBB AABB_EAST_OFF = new AxisAlignedBB(0.3125, 0.0D, 0.0D, 0.6875D, 1.0D, 0.25D);
    protected static final AxisAlignedBB AABB_NORTH_ON = new AxisAlignedBB(0.0D, 0.0D, 0.3125D, 1.0D, 1.0D, 0.6875D);
    protected static final AxisAlignedBB AABB_SOUTH_ON = new AxisAlignedBB(0.0D, 0.0D, 0.3125D, 1.0D, 1.0D, 0.6875D);
    protected static final AxisAlignedBB AABB_WEST_ON = new AxisAlignedBB(0.3125, 0.0D, 0.0D, 0.6875D, 1.0D, 1.0D);
    protected static final AxisAlignedBB AABB_EAST_ON = new AxisAlignedBB(0.3125, 0.0D, 0.0D, 0.6875D, 1.0D, 1.0D);

    private boolean canSustainPlant;

    public BlockBeamMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
    	super(materialIn);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH).withProperty(ACTIVATED, Boolean.valueOf(false)).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }

    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }
    
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        EnumFacing enumfacing = (EnumFacing)state.getValue(FACING);
        boolean flag = ((Boolean)state.getValue(ACTIVATED)).booleanValue();

        switch (enumfacing)
        {
            case EAST:
                return flag ? AABB_EAST_ON : AABB_EAST_OFF;
            case WEST:
                return flag ? AABB_WEST_ON : AABB_WEST_OFF;
            case SOUTH:
                return flag ? AABB_SOUTH_ON : AABB_SOUTH_OFF;
            case NORTH:
            default:
                return flag ? AABB_NORTH_ON : AABB_NORTH_OFF;
        }
    }
    
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
    
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }
    
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
    {
    	EnumType[] allType = EnumType.values();
    	for (EnumType type : allType) 
    	{
    		list.add(new ItemStack(itemIn, 1, type.getMetadata()));
    	}
    }
    
    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
    	boolean flag = worldIn.isBlockPowered(pos);
    	EnumType enumtype = EnumType.byMetadata(meta);
    	EnumFacing enumfacing = placer.getHorizontalFacing().rotateY();
        
        return this.getDefaultState().withProperty(FACING, enumfacing).withProperty(ACTIVATED, Boolean.valueOf(flag)).withProperty(VARIANT, enumtype);    
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (((Boolean)state.getValue(ACTIVATED)).booleanValue())
        {
            state = state.withProperty(ACTIVATED, Boolean.valueOf(false));
            worldIn.setBlockState(pos, state, 10);
        }
        else
        {
            state = state.withProperty(ACTIVATED, Boolean.valueOf(true));
            worldIn.setBlockState(pos, state, 10);
        }

        worldIn.playEvent(playerIn, ((Boolean)state.getValue(ACTIVATED)).booleanValue() ? 1008 : 1014, pos, 0);
        return true;
    }
    
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return true;
    }

    public IBlockState getStateFromMeta(int meta)
    {
    	EnumFacing facing = EnumFacing.getHorizontal(meta);
        int variantbits = (meta & 0x08) >> 3;
        EnumType variant = EnumType.byMetadata(variantbits);
        return this.getDefaultState().withProperty(VARIANT, variant).withProperty(FACING, facing).withProperty(ACTIVATED, Boolean.valueOf((meta & 4) != 0));
    }

    public int getMetaFromState(IBlockState state)
    {
    	EnumFacing facing = (EnumFacing)state.getValue(FACING);
    	EnumType variant = (EnumType)state.getValue(VARIANT);

        int facingbits = facing.getHorizontalIndex();
        if (((Boolean)state.getValue(ACTIVATED)).booleanValue())
        {
        	facingbits |= 4;
        }
        int variantbits = variant.getMetadata() << 3;
        return facingbits | variantbits;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        return state.getBlock() != this ? state : state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }
    
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {ACTIVATED, FACING, VARIANT});
    }

    public int damageDropped(IBlockState state)
    {
        return ((EnumType)state.getValue(VARIANT)).getMetadata();
    }
    
    public static enum EnumType implements IStringSerializable
    {
        ALPHA(0, "alpha"),
        BRAVO(1, "bravo");
    	
    	private static final EnumType[] META_LOOKUP = new EnumType[values().length];
        private final int meta;
        private final String name;
        
        private EnumType(int metaIn, String nameIn)
        {
        	this.meta = metaIn;
        	this.name = nameIn;
        }

        public int getMetadata()
        {
        	return this.meta;
        }

        @Override
        public String toString()
        {
        	return this.name;
        }
        
        public static EnumType byMetadata(int meta)
        {
        	if (meta < 0 || meta > META_LOOKUP.length)
        	{
        		meta = 0;
        	}
        	return META_LOOKUP[meta];
        }
        
        public String getName()
        {
        	return this.name;
        }
        
        static
        {
        	for (EnumType enumtype : values())
        	{
        		META_LOOKUP[enumtype.getMetadata()] = enumtype;
        	}
        }
    }
}
