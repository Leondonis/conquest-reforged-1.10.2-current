package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 11/17/2016.
 */

public class BlockLeavesFull3Meta extends Block implements IMetaBlockName
{
    public static final PropertyEnum<BlockLeavesFull3Meta.EnumType> VARIANT = PropertyEnum.<BlockLeavesFull3Meta.EnumType>create("variant", BlockLeavesFull3Meta.EnumType.class);
    private boolean canSustainPlant;

    public BlockLeavesFull3Meta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
        super(materialIn);
        this.setDefaultState(this.blockState.getBaseState().withProperty(VARIANT, BlockLeavesFull3Meta.EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        this.setLightOpacity(1);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }

    @Override
    public int getLightOpacity(IBlockState state, IBlockAccess world, BlockPos pos)
    {
        if (state == state.withProperty(VARIANT, EnumType.CHARLIE) || state == state.withProperty(VARIANT, EnumType.DELTA) || state == state.withProperty(VARIANT, EnumType.ECHO)  ) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public String getSpecialName(ItemStack stack)
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable)
    {
        return canSustainPlant;
    }

    public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

    public boolean isVisuallyOpaque()
    {
        return false;
    }

	@SideOnly(value=Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT_MIPPED;
    }


    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list)
    {
        for (BlockLeavesFull3Meta.EnumType blockleaves$enumtype : BlockLeavesFull3Meta.EnumType.values())
        {
            list.add(new ItemStack(itemIn, 1, blockleaves$enumtype.getMetadata()));
        }
    }

    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(VARIANT, BlockLeavesFull3Meta.EnumType.byMetadata(meta));
    }

    public int getMetaFromState(IBlockState state)
    {
        return ((BlockLeavesFull3Meta.EnumType)state.getValue(VARIANT)).getMetadata();
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {VARIANT});
    }

    public int damageDropped(IBlockState state)
    {
        return ((BlockLeavesFull3Meta.EnumType)state.getValue(VARIANT)).getMetadata();
    }

    public static enum EnumType implements IStringSerializable
    {
        ALPHA(0, "alpha"),
        BRAVO(1, "bravo"),
        CHARLIE(2, "charlie"),
        DELTA(3, "delta"),
        ECHO(4, "echo"),
        FOX(5, "fox"),
        GOLF(6, "golf"),
        HOTEL(7, "hotel"),
        INDIA(8, "india"),
    	JULIET(9, "juliet"),
    	KILO(10, "kilo"),
    	LIMA(11, "lima"),
    	MIKE(12, "mike"),
    	NOVEMBER(13, "november"),
    	OSCAR(14, "oscar"),
    	PAPA(15, "papa");

        private static final BlockLeavesFull3Meta.EnumType[] META_LOOKUP = new BlockLeavesFull3Meta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockLeavesFull3Meta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockLeavesFull3Meta.EnumType blockleaves$enumtype : values())
            {
                META_LOOKUP[blockleaves$enumtype.getMetadata()] = blockleaves$enumtype;
            }
        }
    }
}