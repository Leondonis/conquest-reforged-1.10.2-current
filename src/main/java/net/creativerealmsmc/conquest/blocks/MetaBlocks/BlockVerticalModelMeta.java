package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 11/21/2016.
 */

public class BlockVerticalModelMeta extends BlockHorizontal implements IMetaBlockName
{
	public static final PropertyEnum<BlockVerticalModelMeta.EnumType> VARIANT = PropertyEnum.<BlockVerticalModelMeta.EnumType>create("variant", BlockVerticalModelMeta.EnumType.class);
    private boolean canSustainPlant;

    public BlockVerticalModelMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
    	super(materialIn);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }
    
    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }
    
    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
    
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }
    
    public boolean isFullCube(IBlockState state)
    {
        return false;
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
    {
    	EnumType[] allType = EnumType.values();
    	for (EnumType type : allType) 
    	{
    		list.add(new ItemStack(itemIn, 1, type.getMetadata()));
    	}
    }
    
    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
    	EnumType enumtype = EnumType.byMetadata(meta);
    	EnumFacing enumfacing = placer.getHorizontalFacing().rotateY();
        
        return this.getDefaultState().withProperty(FACING, enumfacing).withProperty(VARIANT, enumtype);    
    }
    
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return true;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        int facingbits = (meta & 12) >> 2;
    	EnumFacing facing = EnumFacing.getHorizontal(facingbits);
        EnumType variant = EnumType.byMetadata(meta & 3);
        return this.getDefaultState().withProperty(VARIANT, variant).withProperty(FACING, facing);
    }

    public int getMetaFromState(IBlockState state)
    {
    	EnumFacing facing = (EnumFacing)state.getValue(FACING);
    	EnumType variant = (EnumType)state.getValue(VARIANT);

        int facingbits = facing.getHorizontalIndex() << 2;
        int variantbits = variant.getMetadata();
        return facingbits | variantbits;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        return state.getBlock() != this ? state : state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }
    
    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {FACING, VARIANT});
    }

    public int damageDropped(IBlockState state)
    {
        return ((BlockVerticalModelMeta.EnumType)state.getValue(VARIANT)).getMetadata();
    }
    
    public static enum EnumType implements IStringSerializable
    {
    	ALPHA(0, "alpha"),
    	BRAVO(1, "bravo"),
    	CHARLIE(2, "charlie"),
        DELTA(3, "delta");

        private static final BlockVerticalModelMeta.EnumType[] META_LOOKUP = new BlockVerticalModelMeta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockVerticalModelMeta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockVerticalModelMeta.EnumType blockhorizontal$enumtype : values())
            {
                META_LOOKUP[blockhorizontal$enumtype.getMetadata()] = blockhorizontal$enumtype;
            }
        }
    }
}
