package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import com.google.common.collect.Lists;
import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 11/18/2016.
 */

public class BlockFullslitMeta extends Block implements IMetaBlockName
{
    public static final PropertyEnum<BlockFullslitMeta.EnumAxis> AXIS = PropertyEnum.<BlockFullslitMeta.EnumAxis>create("axis", BlockFullslitMeta.EnumAxis.class);
	public static final PropertyEnum<BlockFullslitMeta.EnumType> VARIANT = PropertyEnum.<BlockFullslitMeta.EnumType>create("variant", BlockFullslitMeta.EnumType.class);
    boolean canSustainPlant;

    protected static final AxisAlignedBB topleft_y = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.25D, 1.0D, 0.25D);
    protected static final AxisAlignedBB topright_y = new AxisAlignedBB(0.75D, 0.0D, 0.0D, 1.0D, 1.0D, 0.25D);
    protected static final AxisAlignedBB bottomleft_y = new AxisAlignedBB(0.0D, 0.0D, 0.75D, 0.25D, 1.0D, 1.0D);
    protected static final AxisAlignedBB bottomright_y = new AxisAlignedBB(0.75D, 0.0D, 0.75D, 1.0D, 1.0D, 1.0D);

    protected static final AxisAlignedBB topleft_x = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 0.25D);
    protected static final AxisAlignedBB topright_x = new AxisAlignedBB(0.0D, 0.0D, 0.75D, 1.0D, 0.25D, 1.0D);
    protected static final AxisAlignedBB bottomleft_x = new AxisAlignedBB(0.0D, 0.75D, 0.0D, 1.0D, 1.0D, 0.25D);
    protected static final AxisAlignedBB bottomright_x = new AxisAlignedBB(0.0D, 0.75D, 0.75D, 1.0D, 1.0D, 1.0D);

    protected static final AxisAlignedBB topleft_z = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.25D, 0.25D, 1.0D);
    protected static final AxisAlignedBB topright_z = new AxisAlignedBB(0.75D, 0.0D, 0.0D, 1.0D, 1.0D, 0.25D);
    protected static final AxisAlignedBB bottomleft_z = new AxisAlignedBB(0.0D, 0.75D, 0.0D, 0.25D, 1.0D, 1.0D);
    protected static final AxisAlignedBB bottomright_z = new AxisAlignedBB(0.75D, 0.75D, 0.0D, 1.0D, 1.0D, 1.0D);

    public BlockFullslitMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
        super(materialIn);
        this.setDefaultState(this.blockState.getBaseState().withProperty(AXIS, EnumAxis.Y).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }

    /** Ray traces through the blocks collision from start vector to end vector returning a ray trace hit. */
    @Nullable
    public RayTraceResult collisionRayTrace(IBlockState blockState, World worldIn, BlockPos pos, Vec3d start, Vec3d end) {
        List<RayTraceResult> list = Lists.<RayTraceResult> newArrayList();

        for (AxisAlignedBB axisalignedbb : getCollisionBoxList(this.getActualState(blockState, worldIn, pos))) {
            list.add(this.rayTrace(pos, start, end, axisalignedbb));
        }

        RayTraceResult raytraceresult1 = null;
        double d1 = 0.0D;

        for (RayTraceResult raytraceresult : list) {
            if (raytraceresult != null) {
                double d0 = raytraceresult.hitVec.squareDistanceTo(end);

                if (d0 > d1) {
                    raytraceresult1 = raytraceresult;
                    d1 = d0;
                }
            }
        }

        return raytraceresult1;
    }

    private AxisAlignedBB[] getCollisionBoxList(IBlockState state) {
        switch ((EnumAxis) state.getValue(AXIS)) {
            case Y:
            default:
                return new AxisAlignedBB[] { topleft_y, topright_y, bottomleft_y, bottomright_y };
            case X:
                return new AxisAlignedBB[] { topleft_x, topright_x, bottomleft_x, bottomright_x };
            case Z:
                return new AxisAlignedBB[] { topleft_z, topright_z, bottomleft_z, bottomright_z };
        }
    }

    @Override
    public String getSpecialName(ItemStack stack)
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }

    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        return this.getStateFromMeta(meta).withProperty(AXIS, BlockFullslitMeta.EnumAxis.fromFacingAxis(facing.getAxis()));
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list)
    {
    	for (BlockFullslitMeta.EnumType blockslit$enumtype : BlockFullslitMeta.EnumType.values())
        {
            list.add(new ItemStack(itemIn, 1, blockslit$enumtype.getMetadata()));
        }
    }

    @SideOnly(value=Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return blockAccess.getBlockState(pos.offset(side)).getBlock() != this && super.shouldSideBeRendered(blockState, blockAccess, pos, side);
    }

    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState().withProperty(VARIANT, BlockFullslitMeta.EnumType.byMetadata((meta & 3) % 4));

        switch (meta & 12)
        {
            case 0:
                iblockstate = iblockstate.withProperty(AXIS, BlockFullslitMeta.EnumAxis.Y);
                break;
            case 4:
                iblockstate = iblockstate.withProperty(AXIS, BlockFullslitMeta.EnumAxis.X);
                break;
            case 8:
                iblockstate = iblockstate.withProperty(AXIS, BlockFullslitMeta.EnumAxis.Z);
                break;
            default:
                iblockstate = iblockstate.withProperty(AXIS, BlockFullslitMeta.EnumAxis.NONE);
        }
		return iblockstate;
    }

    public int getMetaFromState(IBlockState state)
    {
        int i = 0;
        i = i | ((BlockFullslitMeta.EnumType)state.getValue(VARIANT)).getMetadata();

        switch ((BlockFullslitMeta.EnumAxis)state.getValue(AXIS))
        {
        	case Y:
        		i |= 0;
        		break;
            case X:
                i |= 4;
                break;
            case Z:
                i |= 8;
                break;
            case NONE:
            	i |= 12;
        }

        return i;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        switch (rot)
        {
            case COUNTERCLOCKWISE_90:
            case CLOCKWISE_90:

                switch ((BlockFullslitMeta.EnumAxis)state.getValue(AXIS))
                {
                    case X:
                        return state.withProperty(AXIS, BlockFullslitMeta.EnumAxis.Z);
                    case Z:
                        return state.withProperty(AXIS, BlockFullslitMeta.EnumAxis.X);
                    default:
                        return state;
                }

            default:
                return state;
        }
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {AXIS, VARIANT});
    }

    public int damageDropped(IBlockState state)
    {
        return ((BlockFullslitMeta.EnumType)state.getValue(VARIANT)).getMetadata();
    }

    public static enum EnumAxis implements IStringSerializable
    {
        X("x"),
        Y("y"),
        Z("z"),
    	NONE("none");

        private final String name;

        private EnumAxis(String name)
        {
            this.name = name;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockFullslitMeta.EnumAxis fromFacingAxis(EnumFacing.Axis axis)
        {
            switch (axis)
            {
            	case X:
            		return X;
            	case Y:
            		return Y;
            	case Z:
            		return Z;
            	default:
            		return NONE;
            }
        }

        public String getName()
        {
            return this.name;
        }
    }

    public static enum EnumType implements IStringSerializable
    {
    	ALPHA(0, "alpha"),
        BRAVO(1, "bravo"),
        CHARLIE(2, "charlie"),
        DELTA(3, "delta");

        private static final BlockFullslitMeta.EnumType[] META_LOOKUP = new BlockFullslitMeta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockFullslitMeta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockFullslitMeta.EnumType blockslit$enumtype : values())
            {
                META_LOOKUP[blockslit$enumtype.getMetadata()] = blockslit$enumtype;
            }
        }
    }
}