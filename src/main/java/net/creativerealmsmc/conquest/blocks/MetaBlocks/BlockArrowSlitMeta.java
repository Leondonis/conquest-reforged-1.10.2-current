package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import com.google.common.collect.Lists;
import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 11/21/2016.
 */

public class BlockArrowSlitMeta extends BlockHorizontal implements IMetaBlockName
{
	public static final PropertyEnum<BlockArrowSlitMeta.EnumType> VARIANT = PropertyEnum.<BlockArrowSlitMeta.EnumType>create("variant", BlockArrowSlitMeta.EnumType.class);
	/* protected static final AxisAlignedBB ex = new AxisAlignedBB(9 / 16, 0.0D, 15 / 16, 13 / 16, 1.0D, 1.0D);//front right U protected static final AxisAlignedBB ex = new AxisAlignedBB(3 / 16, 0.0D, 15 / 16, 7 / 16, 1.0D, 1.0D);//front left protected static final AxisAlignedBB ex = new AxisAlignedBB(13 / 16, 0.0D, 0.5D, 1.0D, 1.0D, 1.0D);//side right protected static final AxisAlignedBB ex = new AxisAlignedBB(0.0D, 0.0D, 0.5D, 3 / 16, 1.0D, 1.0D);//side left */

    protected static final AxisAlignedBB north_fr = new AxisAlignedBB(0.0D, 0.0D, 0.5625D, 0.0625D, 1.0D, 0.8125D);	//west
    protected static final AxisAlignedBB north_fl = new AxisAlignedBB(0.0D, 0.0D, 0.1875D, 0.0625D, 1.0D, 0.4375D);	//west
    protected static final AxisAlignedBB north_sr = new AxisAlignedBB(0.0D, 0.0D, 0.8125D, 0.5D, 1.0D, 1.0D);		//west
    protected static final AxisAlignedBB north_sl = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.5D, 1.0D, 0.1875D);		//west

    protected static final AxisAlignedBB south_fr = new AxisAlignedBB(1.0D, 0.0D, 0.5625D, 0.9375D, 1.0D, 0.8125D);	//east
    protected static final AxisAlignedBB south_fl = new AxisAlignedBB(1.0D, 0.0D, 0.1875D, 0.9375D, 1.0D, 0.4375D);	//east
    protected static final AxisAlignedBB south_sr = new AxisAlignedBB(1.0D, 0.0D, 0.8125D, 0.5D, 1.0D, 1.0D);		//east
    protected static final AxisAlignedBB south_sl = new AxisAlignedBB(1.0D, 0.0D, 0.0D, 0.5D, 1.0D, 0.1875D);		//east

    protected static final AxisAlignedBB west_fr = new AxisAlignedBB(0.5625D, 0.0D, 0.9375D, 0.8125D, 1.0D, 1.0D);	//south
    protected static final AxisAlignedBB west_fl = new AxisAlignedBB(0.1875D, 0.0D, 0.9375D, 0.4375D, 1.0D, 1.0D);	//south
    protected static final AxisAlignedBB west_sr = new AxisAlignedBB(0.8125D, 0.0D, 0.5D, 1.0D, 1.0D, 1.0D);		//south
    protected static final AxisAlignedBB west_sl = new AxisAlignedBB(0.0D, 0.0D, 0.5D, 0.1875D, 1.0D, 1.0D);		//south

    protected static final AxisAlignedBB east_fr = new AxisAlignedBB(0.5625D, 0.0D, 0.0625D, 0.8125D, 1.0D, 0.0D);		//north
    protected static final AxisAlignedBB east_fl = new AxisAlignedBB(0.1875D, 0.0D, 0.0625D, 0.4375D, 1.0D, 0.0D);		//north
    protected static final AxisAlignedBB east_sr = new AxisAlignedBB(0.8125D, 0.0D, 0.5D, 1.0D, 1.0D, 0.0D);			//north
    protected static final AxisAlignedBB east_sl = new AxisAlignedBB(0.0D, 0.0D, 0.5D, 0.1875D, 1.0D, 0.0D);			//north

    private boolean canSustainPlant;

    public BlockArrowSlitMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
    	super(materialIn);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }

    @Override
    public String getSpecialName(ItemStack stack)
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }

    public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, @Nullable Entity entityIn) {
        for (AxisAlignedBB axisalignedbb : getCollisionBoxList(state))
            addCollisionBoxToList(pos, entityBox, collidingBoxes, axisalignedbb);

    }

    /** Ray traces through the blocks collision from start vector to end vector returning a ray trace hit. */
    @Nullable
    public RayTraceResult collisionRayTrace(IBlockState blockState, World worldIn, BlockPos pos, Vec3d start, Vec3d end) {
        List<RayTraceResult> list = Lists.<RayTraceResult> newArrayList();

        for (AxisAlignedBB axisalignedbb : getCollisionBoxList(this.getActualState(blockState, worldIn, pos))) {
            list.add(this.rayTrace(pos, start, end, axisalignedbb));
        }

        RayTraceResult raytraceresult1 = null;
        double d1 = 0.0D;

        for (RayTraceResult raytraceresult : list) {
            if (raytraceresult != null) {
                double d0 = raytraceresult.hitVec.squareDistanceTo(end);

                if (d0 > d1) {
                    raytraceresult1 = raytraceresult;
                    d1 = d0;
                }
            }
        }

        return raytraceresult1;
    }

    private AxisAlignedBB[] getCollisionBoxList(IBlockState state) {
        switch ((EnumFacing) state.getValue(FACING)) {
            case NORTH:
            default:
                return new AxisAlignedBB[] { north_fr, north_fl, north_sr, north_sl };
            case SOUTH:
                return new AxisAlignedBB[] { south_fr, south_fl, south_sr, south_sl };
            case WEST:
                return new AxisAlignedBB[] { west_fr, west_fl, west_sr, west_sl };
            case EAST:
                return new AxisAlignedBB[] { east_fr, east_fl, east_sr, east_sl };
        }
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List list)
    {
    	EnumType[] allType = EnumType.values();
    	for (EnumType type : allType)
    	{
    		list.add(new ItemStack(itemIn, 1, type.getMetadata()));
    	}
    }

    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
    	EnumType enumtype = EnumType.byMetadata(meta);
    	EnumFacing enumfacing = placer.getHorizontalFacing().rotateY();

        return this.getDefaultState().withProperty(FACING, enumfacing).withProperty(VARIANT, enumtype);
    }

    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return true;
    }

    public IBlockState getStateFromMeta(int meta)
    {
        int facingbits = (meta & 12) >> 2;
    	EnumFacing facing = EnumFacing.getHorizontal(facingbits);
        EnumType variant = EnumType.byMetadata(meta & 3);
        return this.getDefaultState().withProperty(VARIANT, variant).withProperty(FACING, facing);
    }

    public int getMetaFromState(IBlockState state)
    {
    	EnumFacing facing = (EnumFacing)state.getValue(FACING);
    	EnumType variant = (EnumType)state.getValue(VARIANT);

        int facingbits = facing.getHorizontalIndex() << 2;
        int variantbits = variant.getMetadata();
        return facingbits | variantbits;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        return state.getBlock() != this ? state : state.withProperty(FACING, rot.rotate((EnumFacing)state.getValue(FACING)));
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {FACING, VARIANT});
    }

    public int damageDropped(IBlockState state)
    {
        return ((BlockArrowSlitMeta.EnumType)state.getValue(VARIANT)).getMetadata();
    }

    public static enum EnumType implements IStringSerializable
    {
    	ALPHA(0, "alpha"),
    	BRAVO(1, "bravo"),
    	CHARLIE(2, "charlie"),
        DELTA(3, "delta");

        private static final BlockArrowSlitMeta.EnumType[] META_LOOKUP = new BlockArrowSlitMeta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockArrowSlitMeta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockArrowSlitMeta.EnumType blockhorizontal$enumtype : values())
            {
                META_LOOKUP[blockhorizontal$enumtype.getMetadata()] = blockhorizontal$enumtype;
            }
        }
    }
}
