package net.creativerealmsmc.conquest.blocks.MetaBlocks;

import net.creativerealmsmc.conquest.items.IMetaBlockName;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.Locale;

/**
 * Created by Artemisia on 11/18/2016.
 */

public class BlockRotatedPillarMeta extends Block implements IMetaBlockName
{
    public static final PropertyEnum<BlockRotatedPillarMeta.EnumAxis> AXIS = PropertyEnum.<BlockRotatedPillarMeta.EnumAxis>create("axis", BlockRotatedPillarMeta.EnumAxis.class);
	public static final PropertyEnum<BlockRotatedPillarMeta.EnumType> VARIANT = PropertyEnum.<BlockRotatedPillarMeta.EnumType>create("variant", BlockRotatedPillarMeta.EnumType.class);
    boolean canSustainPlant;

    public BlockRotatedPillarMeta(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
        super(materialIn);
        this.setDefaultState(this.blockState.getBaseState().withProperty(AXIS, EnumAxis.Y).withProperty(VARIANT, EnumType.ALPHA));
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }
    
    @Override
    public String getSpecialName(ItemStack stack) 
    {
        return EnumType.values()[stack.getItemDamage()].name().toLowerCase(Locale.ENGLISH);
    }
    
    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }
    
    public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        return this.getStateFromMeta(meta).withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.fromFacingAxis(facing.getAxis()));
    }
    
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item itemIn, CreativeTabs tab, List<ItemStack> list)
    {
    	for (BlockRotatedPillarMeta.EnumType blockslit$enumtype : BlockRotatedPillarMeta.EnumType.values())
        {
            list.add(new ItemStack(itemIn, 1, blockslit$enumtype.getMetadata()));
        }
    }
    
    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState().withProperty(VARIANT, BlockRotatedPillarMeta.EnumType.byMetadata((meta & 3) % 4));
        
        switch (meta & 12)
        {
            case 0:
                iblockstate = iblockstate.withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.Y);
                break;
            case 4:
                iblockstate = iblockstate.withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.X);
                break;
            case 8:
                iblockstate = iblockstate.withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.Z);
                break;
            default:
                iblockstate = iblockstate.withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.NONE);
        }
		return iblockstate;
    }
    
    public int getMetaFromState(IBlockState state)
    {
        int i = 0;
        i = i | ((BlockRotatedPillarMeta.EnumType)state.getValue(VARIANT)).getMetadata();
        
        switch ((BlockRotatedPillarMeta.EnumAxis)state.getValue(AXIS))
        {
        	case Y:
        		i |= 0;
        		break;
            case X:
                i |= 4;
                break;
            case Z:
                i |= 8;
                break;
            case NONE:
            	i |= 12;
        }

        return i;
    }

    public IBlockState withRotation(IBlockState state, Rotation rot)
    {
        switch (rot)
        {
            case COUNTERCLOCKWISE_90:
            case CLOCKWISE_90:

                switch ((BlockRotatedPillarMeta.EnumAxis)state.getValue(AXIS))
                {
                    case X:
                        return state.withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.Z);
                    case Z:
                        return state.withProperty(AXIS, BlockRotatedPillarMeta.EnumAxis.X);
                    default:
                        return state;
                }

            default:
                return state;
        }
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {AXIS, VARIANT});
    }
    
    public int damageDropped(IBlockState state)
    {
        return ((BlockRotatedPillarMeta.EnumType)state.getValue(VARIANT)).getMetadata();
    }

    public static enum EnumAxis implements IStringSerializable
    {
        X("x"),
        Y("y"),
        Z("z"),
    	NONE("none");

        private final String name;

        private EnumAxis(String name)
        {
            this.name = name;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockRotatedPillarMeta.EnumAxis fromFacingAxis(EnumFacing.Axis axis)
        {
            switch (axis)
            {
            	case X:
            		return X;
            	case Y:
            		return Y;
            	case Z:
            		return Z;
            	default:
            		return NONE;
            }
        }

        public String getName()
        {
            return this.name;
        }
    }
    
    public static enum EnumType implements IStringSerializable
    {
    	ALPHA(0, "alpha"),
        BRAVO(1, "bravo"),
        CHARLIE(2, "charlie"),
        DELTA(3, "delta");

        private static final BlockRotatedPillarMeta.EnumType[] META_LOOKUP = new BlockRotatedPillarMeta.EnumType[values().length];
        private final int meta;
        private final String name;

        private EnumType(int metaIn, String nameIn)
        {
            this.meta = metaIn;
            this.name = nameIn;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockRotatedPillarMeta.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        static
        {
            for (BlockRotatedPillarMeta.EnumType blockslit$enumtype : values())
            {
                META_LOOKUP[blockslit$enumtype.getMetadata()] = blockslit$enumtype;
            }
        }
    }
}