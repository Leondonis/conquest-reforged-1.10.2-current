package net.creativerealmsmc.conquest.blocks;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFlowerPot;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Phil on 9/24/2016.
 */
public class FlowerpotBlock extends BlockContainer
{
    private boolean canSustainPlant;
    public static final PropertyEnum<FlowerpotBlock.EnumFlowerType> CONTENTS = PropertyEnum.<FlowerpotBlock.EnumFlowerType>create("contents", FlowerpotBlock.EnumFlowerType.class);
    protected static final AxisAlignedBB FLOWER_POT_AABB = new AxisAlignedBB(0.3125D, 0.0D, 0.3125D, 0.6875D, 0.375D, 0.6875D);

    public FlowerpotBlock(String nameIn, String unlocalizedNameIn, Material materialIn, float hardness, float resistance, SoundType sound, float lightLevel, String tool, int toolLevel, boolean canSustainPlant, CreativeTabs tab)
    {
        super(materialIn);
        this.setRegistryName(nameIn);
        this.setUnlocalizedName(unlocalizedNameIn);
        this.setCreativeTab(tab);
        this.setHardness(hardness);
        this.setResistance(resistance);
        this.setSoundType(sound);
        this.setLightLevel(lightLevel);
        this.setDefaultState(this.blockState.getBaseState().withProperty(CONTENTS, FlowerpotBlock.EnumFlowerType.EMPTY));
        if(tool != null)
        {
            this.setHarvestLevel(tool, toolLevel);
        }
        this.canSustainPlant = canSustainPlant;
    }

    public boolean canSustainPlant(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing direction, net.minecraftforge.common.IPlantable plantable)
    {
        return canSustainPlant;
    }
    
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        return FLOWER_POT_AABB;
    }

    /**
     * Used to determine ambient occlusion and culling when rebuilding chunks for render
     */
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    /**
     * The type of render function called. 3 for standard block models, 2 for TESR's, 1 for liquids, -1 is no render
     */
    public EnumBlockRenderType getRenderType(IBlockState state)
    {
        return EnumBlockRenderType.MODEL;
    }

    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (heldItem != null && heldItem.getItem() instanceof ItemBlock)
        {
            TileEntityFlowerPot tileentityflowerpot = this.getTileEntity(worldIn, pos);

            if (tileentityflowerpot == null)
            {
                return false;
            }
            else if (tileentityflowerpot.getFlowerPotItem() != null)
            {
                return false;
            }
            else
            {
                Block block = Block.getBlockFromItem(heldItem.getItem());

                if (!this.canContain(block, heldItem.getMetadata()))
                {
                    return false;
                }
                else
                {
                    tileentityflowerpot.setFlowerPotData(heldItem.getItem(), heldItem.getMetadata());
                    tileentityflowerpot.markDirty();
                    worldIn.notifyBlockUpdate(pos, state, state, 3);
                    playerIn.addStat(StatList.FLOWER_POTTED);

                    if (!playerIn.capabilities.isCreativeMode)
                    {
                        --heldItem.stackSize;
                    }

                    return true;
                }
            }
        }
        else
        {
            return false;
        }
    }

    private boolean canContain(Block blockIn, int meta)
    {
        return !(blockIn != Blocks.YELLOW_FLOWER
                && blockIn != Blocks.RED_FLOWER
                && blockIn != Blocks.CACTUS
                && blockIn != Blocks.BROWN_MUSHROOM
                && blockIn != Blocks.RED_MUSHROOM
                && blockIn != Blocks.SAPLING
                && blockIn != Blocks.DEADBUSH)
                || blockIn == Blocks.TALLGRASS && meta == BlockTallGrass.EnumType.FERN.getMeta();
    }

    public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
    {
        return true;
    }

    /**
     * Called when a neighboring block changes.
     */
    public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock)
    {
        if (!worldIn.getBlockState(pos.down()).isFullyOpaque())
        {
            this.dropBlockAsItem(worldIn, pos, state, 0);
            worldIn.setBlockToAir(pos);
        }
    }

    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        super.breakBlock(worldIn, pos, state);
    }

    public void onBlockHarvested(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player)
    {
        super.onBlockHarvested(worldIn, pos, state, player);

        if (player.capabilities.isCreativeMode)
        {
            TileEntityFlowerPot tileentityflowerpot = this.getTileEntity(worldIn, pos);

            if (tileentityflowerpot != null)
            {
                tileentityflowerpot.setFlowerPotData((Item)null, 0);
            }
        }
    }

    /**
     * Get the Item that this Block should drop when harvested.
     */

    private TileEntityFlowerPot getTileEntity(World worldIn, BlockPos pos)
    {
        TileEntity tileentity = worldIn.getTileEntity(pos);
        return tileentity instanceof TileEntityFlowerPot ? (TileEntityFlowerPot)tileentity : null;
    }

    /**
     * Returns a new instance of a block's tile entity class. Called on placing the block.
     */
    public TileEntity createNewTileEntity(World worldIn, int meta)
    {
        Block block = null;
        int i = 0;

        switch (meta)
        {
            case 1:
                block = Blocks.RED_FLOWER;
                i = BlockFlower.EnumFlowerType.POPPY.getMeta();
                break;
            case 2:
                block = Blocks.YELLOW_FLOWER;
                break;
            case 3:
                block = Blocks.SAPLING;
                i = BlockPlanks.EnumType.OAK.getMetadata();
                break;
            case 4:
                block = Blocks.SAPLING;
                i = BlockPlanks.EnumType.SPRUCE.getMetadata();
                break;
            case 5:
                block = Blocks.SAPLING;
                i = BlockPlanks.EnumType.BIRCH.getMetadata();
                break;
            case 6:
                block = Blocks.SAPLING;
                i = BlockPlanks.EnumType.JUNGLE.getMetadata();
                break;
            case 7:
                block = Blocks.RED_MUSHROOM;
                break;
            case 8:
                block = Blocks.BROWN_MUSHROOM;
                break;
            case 9:
                block = Blocks.CACTUS;
                break;
            case 10:
                block = Blocks.DEADBUSH;
                break;
            case 11:
                block = Blocks.TALLGRASS;
                i = BlockTallGrass.EnumType.FERN.getMeta();
                break;
            case 12:
                block = Blocks.SAPLING;
                i = BlockPlanks.EnumType.ACACIA.getMetadata();
                break;
            case 13:
                block = Blocks.SAPLING;
                i = BlockPlanks.EnumType.DARK_OAK.getMetadata();
        }

        return new TileEntityFlowerPot(Item.getItemFromBlock(block), i);
    }

    protected BlockStateContainer createBlockState()
    {
        return new BlockStateContainer(this, new IProperty[] {CONTENTS});
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    public int getMetaFromState(IBlockState state)
    {
        return 0;
    }

    /**
     * Get the actual Block state of this Block at the given position. This applies properties not visible in the
     * metadata, such as fence connections.
     */
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        FlowerpotBlock.EnumFlowerType Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.EMPTY;
        TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof TileEntityFlowerPot)
        {
            TileEntityFlowerPot tileentityflowerpot = (TileEntityFlowerPot)tileentity;
            Item item = tileentityflowerpot.getFlowerPotItem();

            if (item instanceof ItemBlock)
            {
                int i = tileentityflowerpot.getFlowerPotData();
                Block block = Block.getBlockFromItem(item);

                if (block == Blocks.SAPLING)
                {
                    switch (BlockPlanks.EnumType.byMetadata(i))
                    {
                        case OAK:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.OAK_SAPLING;
                            break;
                        case SPRUCE:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.SPRUCE_SAPLING;
                            break;
                        case BIRCH:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.BIRCH_SAPLING;
                            break;
                        case JUNGLE:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.JUNGLE_SAPLING;
                            break;
                        case ACACIA:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.ACACIA_SAPLING;
                            break;
                        case DARK_OAK:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.DARK_OAK_SAPLING;
                            break;
                        default:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.EMPTY;
                    }
                }
                else if (block == Blocks.TALLGRASS)
                {
                    switch (i)
                    {
                        case 0:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.DEAD_BUSH;
                            break;
                        case 2:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.FERN;
                            break;
                        default:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.EMPTY;
                    }
                }
                else if (block == Blocks.YELLOW_FLOWER)
                {
                    Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.DANDELION;
                }
                else if (block == Blocks.RED_FLOWER)
                {
                    switch (BlockFlower.EnumFlowerType.getType(BlockFlower.EnumFlowerColor.RED, i))
                    {
                        case POPPY:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.POPPY;
                            break;
                        case BLUE_ORCHID:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.BLUE_ORCHID;
                            break;
                        case ALLIUM:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.ALLIUM;
                            break;
                        case HOUSTONIA:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.HOUSTONIA;
                            break;
                        case RED_TULIP:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.RED_TULIP;
                            break;
                        case ORANGE_TULIP:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.ORANGE_TULIP;
                            break;
                        case WHITE_TULIP:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.WHITE_TULIP;
                            break;
                        case PINK_TULIP:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.PINK_TULIP;
                            break;
                        case OXEYE_DAISY:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.OXEYE_DAISY;
                            break;
                        default:
                            Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.EMPTY;
                    }
                }
                else if (block == Blocks.RED_MUSHROOM)
                {
                    Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.MUSHROOM_RED;
                }
                else if (block == Blocks.BROWN_MUSHROOM)
                {
                    Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.MUSHROOM_BROWN;
                }
                else if (block == Blocks.DEADBUSH)
                {
                    Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.DEAD_BUSH;
                }
                else if (block == Blocks.CACTUS)
                {
                    Flowerpot$enumflowertype = FlowerpotBlock.EnumFlowerType.CACTUS;
                }
            }
        }

        return state.withProperty(CONTENTS, Flowerpot$enumflowertype);
    }

    @SideOnly(Side.CLIENT)
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }


    /*============================FORGE START=====================================*/
    @Override
    public java.util.List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
        java.util.List<ItemStack> ret = super.getDrops(world, pos, state, fortune);
        TileEntityFlowerPot te = world.getTileEntity(pos) instanceof TileEntityFlowerPot ? (TileEntityFlowerPot)world.getTileEntity(pos) : null;
        if (te != null && te.getFlowerPotItem() != null)
            ret.add(new ItemStack(te.getFlowerPotItem(), 1, te.getFlowerPotData()));
        return ret;
    }
    @Override
    public boolean removedByPlayer(IBlockState state, World world, BlockPos pos, EntityPlayer player, boolean willHarvest)
    {
        if (willHarvest) return true; //If it will harvest, delay deletion of the block until after getDrops
        return super.removedByPlayer(state, world, pos, player, willHarvest);
    }
    @Override
    public void harvestBlock(World world, EntityPlayer player, BlockPos pos, IBlockState state, TileEntity te, ItemStack tool)
    {
        super.harvestBlock(world, player, pos, state, te, tool);
        world.setBlockToAir(pos);
    }
    /*===========================FORGE END==========================================*/

    public static enum EnumFlowerType implements IStringSerializable
    {
        EMPTY("empty"),
        POPPY("rose"),
        BLUE_ORCHID("blue_orchid"),
        ALLIUM("allium"),
        HOUSTONIA("houstonia"),
        RED_TULIP("red_tulip"),
        ORANGE_TULIP("orange_tulip"),
        WHITE_TULIP("white_tulip"),
        PINK_TULIP("pink_tulip"),
        OXEYE_DAISY("oxeye_daisy"),
        DANDELION("dandelion"),
        OAK_SAPLING("oak_sapling"),
        SPRUCE_SAPLING("spruce_sapling"),
        BIRCH_SAPLING("birch_sapling"),
        JUNGLE_SAPLING("jungle_sapling"),
        ACACIA_SAPLING("acacia_sapling"),
        DARK_OAK_SAPLING("dark_oak_sapling"),
        MUSHROOM_RED("mushroom_red"),
        MUSHROOM_BROWN("mushroom_brown"),
        DEAD_BUSH("dead_bush"),
        FERN("fern"),
        CACTUS("cactus");

        private final String name;

        private EnumFlowerType(String name)
        {
            this.name = name;
        }

        public String toString()
        {
            return this.name;
        }

        public String getName()
        {
            return this.name;
        }
    }
}
