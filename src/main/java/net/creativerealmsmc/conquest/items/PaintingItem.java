package net.creativerealmsmc.conquest.items;

import net.creativerealmsmc.conquest.entity.painting.Art;
import net.creativerealmsmc.conquest.entity.painting.IPaintingSupplier;
import net.creativerealmsmc.conquest.entity.painting.PaintingBase;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemHangingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.*;

/**
 * @author dags <dags@dags.me>
 */

/*
 * Recipe for new painting items:
 * 1. Must have class extending PaintingBase
 * 2. Must have implementation of IPaintingSupplier that instantiates said extending class
 * 3. Entity registry name (${name}) is the name of the extending class in lowercase (see getPaintingName() method below)
 * 4. Asset locations:
 *   1. MODID:models/item/${name}.json
 *   2. MODID:textures/items/${name}.png
 *   3. MODID:textures/paintings/${name}.png
 */
public class PaintingItem extends ItemHangingEntity {

    private static final Map<String, PaintingItem> paintingItemMap = new HashMap<String, PaintingItem>();
    private static final List<PaintingItem> orderedItems = new ArrayList<PaintingItem>();

    private final IPaintingSupplier paintingCreator;
    private final String name;

    public PaintingItem(Class<? extends PaintingBase> entityClass, IPaintingSupplier paintingCreator, String domain, String name) {
        super(entityClass);
        this.name = name;
        this.paintingCreator = paintingCreator;
        this.setUnlocalizedName(name);
        this.setRegistryName(domain + ":" + name);
        this.setMaxStackSize(1);

        PaintingItem.paintingItemMap.put(name, this);
        PaintingItem.orderedItems.add(this);
        Collections.sort(orderedItems, comparator);
    }

    @Override
    public void getSubItems(Item itemIn, CreativeTabs tab, List<ItemStack> subItems) {
        if (tab == CreativeTabs.SEARCH) {
            for (Art art : Art.values()) {
                ItemStack stack = new ItemStack(itemIn, 1, art.index());
                subItems.add(stack);
            }
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        return super.getUnlocalizedName() + "." + getArtName(stack);
    }

    public String getName() {
        return name;
    }

    public String getArtName(ItemStack itemStack) {
        return Art.fromId(itemStack.getMetadata()).shapeId;
    }

    @Override
    public EnumActionResult onItemUse(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if(side != EnumFacing.DOWN && side != EnumFacing.UP) {
            BlockPos blockpos = pos.offset(side);

            Art art = Art.fromId(stack.getMetadata());

            if (!player.canPlayerEdit(blockpos, side, stack)) {
                return EnumActionResult.FAIL;
            }

            PaintingBase painting = paintingCreator.createEntity(world, blockpos, side, art);

            if (painting != null) {

                if (!world.isRemote) {
                    if (world.spawnEntityInWorld(painting)) {
                        painting.playPlaceSound();
                    }
                }

                --stack.stackSize;
            }

            return EnumActionResult.SUCCESS;
        }
        return EnumActionResult.FAIL;
    }

    public static Collection<PaintingItem> getItems() {
        return Collections.unmodifiableCollection(orderedItems);
    }

    public static Collection<String> getPaintingIds() {
        return Collections.unmodifiableCollection(paintingItemMap.keySet());
    }

    public static Collection<String> getLocalizedNames() {
        List<String> names = new ArrayList<String>();
        for (PaintingItem item : paintingItemMap.values()) {
            List<ItemStack> list = new ArrayList<ItemStack>();
            item.getSubItems(item, CreativeTabs.SEARCH, list);
            for (ItemStack stack : list) {
                names.add(stack.getDisplayName().toLowerCase());
            }
        }
        return names;
    }

    public static PaintingItem getPainting(String name) {
        return paintingItemMap.get(name);
    }

    private static Comparator<PaintingItem> comparator = new Comparator<PaintingItem>() {
        @Override
        public int compare(PaintingItem o1, PaintingItem o2) {
            return o1.name.compareTo(o2.name);
        }
    };
}
