package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class FurnitureTab extends CreativeTabs {

    public FurnitureTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //vanillaTab.displayAllRelevantItems(items);
        //  Furniture  //
        items.add(new ItemStack(Blocks.FURNACE,1,0)); //Furnace
        items.add(new ItemStack(MetaBlocks.furnace_cobblelit,1,0));//Flaming Regular Furnace
        items.add(new ItemStack(MetaBlocks.furnace_iron,1,0));//Iron Furnace
        items.add(new ItemStack(MetaBlocks.furnace_ironlit,1,0));//Flaming Iron Furnace
        items.add(new ItemStack(MetaBlocks.furnace_sandstone,1,0));//Sandstone Furnace
        items.add(new ItemStack(MetaBlocks.furnace_sandstonelit,1,0));//Flaming Sandstone Furnace
        items.add(new ItemStack(MetaBlocks.hanger_clothes,1,0));//Clothes Hanger
        items.add(new ItemStack(MetaBlocks.wood_verticalslab_32,1,3));//Towel Rack
        items.add(new ItemStack(MetaBlocks.wood_log_4,1,3));//Shelves
        items.add(new ItemStack(MetaBlocks.wood_log_5,1,0));//Spice Rack
        items.add(new ItemStack(MetaBlocks.wood_log_5,1,1));//Scroll Rack
        items.add(new ItemStack(MetaBlocks.wood_log_8,1,3)); //Empty Scroll Rack
        items.add(new ItemStack(MetaBlocks.wood_log_5,1,2));//Cupboards
        items.add(new ItemStack(Blocks.BOOKSHELF,1,0)); //Bookshelf
        items.add(new ItemStack(MetaBlocks.wood_log_7,1,2)); //Elven Bookshelf
        items.add(new ItemStack(MetaBlocks.wood_log_7,1,3)); //Elven Empty Bookshelf
        items.add(new ItemStack(MetaBlocks.wood_log_8,1,0)); //Elven Cabinet
        items.add(new ItemStack(MetaBlocks.wood_log_8,1,1)); //Elven Cabinet 1
        items.add(new ItemStack(MetaBlocks.wood_log_8,1,2)); //Elven Cabinet 2
        items.add(new ItemStack(MetaBlocks.wood_log_9,1,0)); //Mithlond Drawers
        items.add(new ItemStack(MetaBlocks.wood_log_9,1,1)); //Harlond Drawers
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,2)); //Fancy Wardrobe
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,3)); //Simpleton Wardrobe

        items.add(new ItemStack(MetaBlocks.wood_full_6,1,13));//Kitchen Table with Tools
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,14));//Workbench
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_5,1,0));//Wooden Desk
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_8,1,0));//Wooden Table
        items.add(new ItemStack(MetaBlocks.wood_connectedxz_1,1,0));//Table
        items.add(new ItemStack(MetaBlocks.wood_connectedxz_1,1,1));//Table 2
        items.add(new ItemStack(MetaBlocks.wood_connectedxz_1,1,2));//Elven Table
        items.add(new ItemStack(Blocks.CRAFTING_TABLE,1,0)); //Crafting Table
        items.add(new ItemStack(MetaBlocks.wood_slab_1,1,3));//Map Table
        items.add(new ItemStack(Blocks.ENCHANTING_TABLE,1,0)); //Enchantment Table
        items.add(new ItemStack(MetaBlocks.wood_enchantedbook_1,1,0));//Enchanting Table with Legs
        items.add(new ItemStack(MetaBlocks.wood_enchantedbook_1,1,1));//Floating Book
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_4,1,0));//Spruce Chair
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_10,1,1));//Fancy Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,0));//Wicker Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,1));//Wood Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,9));//Wooden Stool
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,2));//Red Cushion Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,3));//Blue Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,4));//Black Cushion Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,5));//Green Cushion Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,6));//Leather Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,7));//Mesh Chair
        items.add(new ItemStack(MetaBlocks.wood_half_1,1,8));//Thatch Chair
        items.add(new ItemStack(MetaBlocks.bed_wolf,1,0));//Wolf Fur Bed
        items.add(new ItemStack(MetaBlocks.bed_rustic,1,0));//Rustic Bed
        items.add(new ItemStack(MetaBlocks.bed_bear,1,0));//Bear Fur Bed
        items.add(new ItemStack(MetaBlocks.bed_fancygreen,1,0));//Fancy Green Bed
        items.add(new ItemStack(MetaBlocks.bed_fancyblue,1,0));//Fancy Blue Bed
        items.add(new ItemStack(MetaBlocks.bed_fancywhite,1,0));//Fancy White Bed
        items.add(new ItemStack(Items.BED)); //Bed
        items.add(new ItemStack(MetaBlocks.bed_poor,1,0));//Rustic Wooden Bed
        items.add(new ItemStack(Blocks.CHEST,1,0)); //Chest
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_4,1,0)); //Chest
        items.add(new ItemStack(Items.SKULL,1,3)); //Small Chest
        items.add(new ItemStack(Blocks.TRAPPED_CHEST,1,0)); //Trapped Chest
        items.add(new ItemStack(Blocks.ENDER_CHEST,1,0)); //Ender CHest
        items.add(new ItemStack(Items.ARMOR_STAND)); //Armor Stand
        items.add(new ItemStack(Blocks.JUKEBOX,1,0)); //Jukebox
        items.add(new ItemStack(Blocks.NOTEBLOCK,1,0)); //Note Block

        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Items.BED;
    }


}