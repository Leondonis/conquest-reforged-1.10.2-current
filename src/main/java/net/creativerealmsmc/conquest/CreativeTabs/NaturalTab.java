package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class NaturalTab extends CreativeTabs {

    public NaturalTab(int index, String newLabel) {
        // Use the same index as the Vanilla tab we want to replace
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //vanillaTab.displayAllRelevantItems(items);
         // Wood //
        items.add(new ItemStack(Blocks.LOG,1,0)); //Oak Wood
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,1));//Connecting Oak Log
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,2));//Mossy Oak Log
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,8)); //Log Oak Ivy
        items.add(new ItemStack(Blocks.LOG,1,1)); //Spruce Wood
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,4));//Connecting Spruce Wood Log
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,5));//Mossy Spruce Log
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,13));//Pine Log 1
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,12));//Pine Log
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,11));//Red and Gray Pine Log
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,10));//Red Pine Log
        items.add(new ItemStack(Blocks.LOG,1,2)); //Birch Wood
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,7));//Connecting Birch Log
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,8));//Mossy Birch Log
        items.add(new ItemStack(Blocks.LOG,1,3)); //Jungle Wood
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,10));//Connecting Jungle Logs
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,11));//Mossy Jungle Log
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,9));//Goat Sallow Log
        items.add(new ItemStack(Blocks.LOG2,1,0)); //Acacia Wood
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,13));//Connecting Acacia Log
        items.add(new ItemStack(MetaBlocks.wood_full_1,1,14));//Mossy Acacia Log
        items.add(new ItemStack(Blocks.LOG2,1,1)); //Dark Oak Wood
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,0));//Connecting Dark Oak Log
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,1));//Mossy Dark Oak Log
        items.add(new ItemStack(MetaBlocks.wood_log_7,1,0));//Beech Log
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,10));//Beech Mossy Log
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,2));//Burnt Ash Log
        items.add(new ItemStack(MetaBlocks.wood_fullpartial_1,1,2));//Palm Tree

        //  Birds  //
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,0)); //Raven
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,1));//Hawk
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,2));//Owl
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,3));//Seagull
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,4));//Mallard Duck
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,5));//Pidgeon
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,6)); //Bluejay
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,7));//Bat

        //  Land Animals //
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,8));//Rat
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,9));//Toad

        //Bugs
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,6)); //Red Butterfly
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,7)); //Blue Butterfly
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,4)); //Dragon Flies
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,10));//Flies
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollisionlight5_1,1,0));//Fireflies
        //  Leaves  //
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,7)); //Floating Willow Leaves
        items.add(new ItemStack(Blocks.LEAVES,1,0)); //Oak Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,7));//Oak Leaves (Extreme Hills)
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,8));//Oak Leaves (Frozen Ocean)
        items.add(new ItemStack(Blocks.LEAVES,1,1)); //Spruce Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,1));//Brown Twigs Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,9));//Spruce Needles Light Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,10));//Spruce Needles Dark Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,12));//Pine Needles Leaves

        items.add(new ItemStack(Blocks.LEAVES,1,2)); //Birch Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,12));//Birch Leaves (Frozen Ocean)
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,13));//Birch Leaves (Extreme Hills)
        items.add(new ItemStack(Blocks.LEAVES,1,3)); //Jungle Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,10));//Jungle Leaves (Taiga)
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,11));//Jungle Leaves (Frozen Ocean)
        items.add(new ItemStack(Blocks.LEAVES2,1,0)); //Acacia Leaves
        items.add(new ItemStack(Blocks.LEAVES2,1,1)); //Dark Oak Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,9));//Dark Oak Leaves (Frozen Ocean)
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,13));//Dark Oak Leaves (Taiga)
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,14));//Aspen Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,15));//Aspen Autumn Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,0));//Rowan Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,2));//Mistletoe Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,3));//Gorse Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,5));//Weeping Willow Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,8));//Downy Willow Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,6));//Horse Chesnut Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,11));//Salix Caprea Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,0));//Green Larch Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,1));//Yellow Larch Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,2));//Bright Mallorn Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,3));//Faded Mallorn Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,4));//Mallorn Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,6));//Lilac Leaves

        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,0));//Cherry Blossoms
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,14));//White Cherry Blossoms
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,15));//Red Cherry Blossoms
        items.add(new ItemStack(MetaBlocks.leaves_full_3,1,5));//Purple Cherry Blossoms

        items.add(new ItemStack(MetaBlocks.leaves_fullbiome_1,1,0));//Bright Apple Tree Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,4));//Dark Broad Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,1));//Apple Tree Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,2));//Holly Berry Bush
        items.add(new ItemStack(MetaBlocks.leaves_full_2,1,7));//Holly Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,3));//Citrus tree Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,4));//Pear Tree Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,5));//Blue Berry Bush Leaves
        items.add(new ItemStack(MetaBlocks.leaves_full_1,1,6));//Grape Vines

        //  Saplings and Tree Stuff  //
        items.add(new ItemStack(Blocks.SAPLING,1,0)); //Oak Sapling
        items.add(new ItemStack(Blocks.SAPLING,1,1)); //Spruce Sapling
        items.add(new ItemStack(Blocks.SAPLING,1,2)); //Birch Sapling
        items.add(new ItemStack(Blocks.SAPLING,1,3)); //Jungle Sapling
        items.add(new ItemStack(Blocks.SAPLING,1,4)); //Acacia Sapling
        items.add(new ItemStack(Blocks.SAPLING,1,5)); //Dark Oak Sapling
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,6)); //Small Tree
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,3)); //Frozen Tree
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,9)); //Cypress
        items.add(new ItemStack(MetaBlocks.wood_fullpartial_1,1,15));//Tree Stump

        //  Flowers and Bushes  //
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,11));//Hawthorn Bush
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,8));//Beautyberry Bushes
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,9));//Raspberry Bush
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,10));//Blackberry Bush
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,14)); //BilBerry Bush
        items.add(new ItemStack(Blocks.TALLGRASS,1,1)); //Tall Grass
        items.add(new ItemStack(Blocks.DOUBLE_PLANT,1,2)); //Double Tall Grass
        items.add(new ItemStack(Blocks.TALLGRASS,1,2)); //Fern
        items.add(new ItemStack(Blocks.DOUBLE_PLANT,1,3)); //Double Tall Fern
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,8));//Hart's Tongue
        items.add(new ItemStack(Blocks.YELLOW_FLOWER,1,0)); //Dandelion
        items.add(new ItemStack(Blocks.RED_FLOWER,1,0)); //Poppy
        items.add(new ItemStack(Blocks.RED_FLOWER,1,1)); //Blue Orchid
        items.add(new ItemStack(Blocks.RED_FLOWER,1,2)); //Allium
        items.add(new ItemStack(Blocks.RED_FLOWER,1,3)); //Azure Bluet
        items.add(new ItemStack(Blocks.RED_FLOWER,1,4)); //Red Tulip
        items.add(new ItemStack(Blocks.RED_FLOWER,1,5)); //Orange Tulip
        items.add(new ItemStack(Blocks.RED_FLOWER,1,6)); //White Tulip
        items.add(new ItemStack(Blocks.RED_FLOWER,1,7)); //Pink Tulip
        items.add(new ItemStack(Blocks.RED_FLOWER,1,8)); //Oxeye Daisy
        items.add(new ItemStack(Blocks.DOUBLE_PLANT,1,0)); //SunFlower
        items.add(new ItemStack(Blocks.DOUBLE_PLANT,1,1)); //Lilac
        items.add(new ItemStack(Blocks.DOUBLE_PLANT,1,4)); //Rose Bush
        items.add(new ItemStack(Blocks.DOUBLE_PLANT,1,5)); //Peony
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,5));//Tall Cow Parsley
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,6));//Lily of the Valley
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,7));//Euphorbia Esula
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,8));//Pulsatilla Vulgaris
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,9));//Knabenkraut
        items.add(new ItemStack(MetaBlocks.plants_nocollisionbiome_1,1,2));//Thick Fern
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,10));//Cicerbita Alpina
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,11));//Sweet Grass
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,12));//Nettles
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,7));//Nettles Wild Overgrown
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,13));//Heather
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,14));//Cotton Grass
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,15));//Hemp
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,0));//Cat Tails
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,0));//Hanging Flowers
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,1));//Hanging Flowers
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,10)); //Autumn Reeds
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,11)); //Bog Asphode
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,12)); //Cow Wheat
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,13)); //Buttercups
        items.add(new ItemStack(MetaBlocks.plants_nocollision_4,1,15)); //Rushes
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,3)); //Green Sedges
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,5)); //Mountain Birch
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,0)); //Fireweed
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,2)); //Summer Reeds
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,5)); //White Clematis
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,1));//Dead Bracken
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,2));//Normal Bracken
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,15));//Autumnal Bracken
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,9));//Dark Red Bracken
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,5));//Angelica
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,10));//Aconitum Lycoctonum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,15));//Arcotostaphylos Alpina
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,0));//Athelas
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,0));//Trefoil Birds Foot
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,12));//Wild Blue Flowers
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,11));//Wild Flower
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,8));//Sphagnum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,1));//Red Sphagnum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,10));//Campanula Rotundifolia
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,7));//Centaurea Nigra
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,6));//Cirsium Heterophyllum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,10));//Dead Shrubs
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,9));//Equisetum Sylvaticum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,11));//Ericatetralix
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,3));//Festuca Pratensis
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,1));//Forest Grass
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,0));//Purple Moor Grass
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,14));//Deschampia Flexuosa
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,12));//Gallium Modoratum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,7));//Gallium Verum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,12));//Wormwood
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,0));//Harlond Flower
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,15));//Harlond Flower 2
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,11));//Harlond Marsh Flowers
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,12));//Mandragora Autumnalis
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,2));//Mithlond Flower
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,13));//Mithlond Flower 2
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,6));//Vaccinium Uliginosum
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,1));//Vaccinium Vitis Idaea
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,11));//Nightshade
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,11));//Phleum Pratense
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,13));//Rock Rose
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,14));//Rumex Longifolius
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,4));//Scabius
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,7));//Sedge
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,3));//Symbelmyne
        items.add(new ItemStack(MetaBlocks.plants_nocollision_9,1,5));//Gold Symbelmyne
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,8));//Thyme
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,9));//Tobacco
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,12));//Pine Cones
        items.add(new ItemStack(MetaBlocks.plants_nocollision_10,1,8));//Spruce Cones
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,4));//Melampyrum Pratense Red
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,2));//Flax
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,8));//Water Mint



        //  Vines  //
        items.add(new ItemStack(Blocks.VINE,1,0)); //Vines
        items.add(new ItemStack(MetaBlocks.vine_jungle,1,0));//Jungle Vines
        items.add(new ItemStack(MetaBlocks.vine_ivy,1,0));//Ivy Vines
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,9));//Hanging Ivy
        items.add(new ItemStack(MetaBlocks.vine_moss,1,0));//Hanging Moss (Vines)
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,10));//Hanging Moss
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,8)); //Hanging Roots

        //  Crops  //
        items.add(new ItemStack(Blocks.PUMPKIN,1,0)); //Pumpkin
        items.add(new ItemStack(Blocks.LIT_PUMPKIN,1,0)); //Lit Pumpkin
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,9));//Barley
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,13));//Mature Wheat
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,14));//Small Wheat
        items.add(new ItemStack(MetaBlocks.plants_nocollision_1,1,15));//Wheat
        items.add(new ItemStack(MetaBlocks.plants_nocollision_11,1,7));//Rice
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,0));//Corn
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,9));//Legumes
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,1));//Beans
        items.add(new ItemStack(MetaBlocks.plants_nocollision_8,1,13));//Beans Pole
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,7));//Cabbage
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,10));//Turnips
        items.add(new ItemStack(MetaBlocks.ground_layer_22,1,1));//Fodder
        items.add(new ItemStack(MetaBlocks.ground_layer_14,1,0));//Dried Fodder


        //  Bamboo  //
        items.add(new ItemStack(MetaBlocks.wood_fence_3,1,9));//Dry Bamboo
        items.add(new ItemStack(MetaBlocks.wood_fence_1,1,0));//Bamboo
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,2));//Bamboo
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,3));//Dark Bamboo

        //  More Plants  //
        items.add(new ItemStack(MetaBlocks.plants_nocollisionbiome_1,1,0));//Jungle Fern
        items.add(new ItemStack(Blocks.CACTUS,1,0)); //Cactus
        items.add(new ItemStack(Blocks.DEADBUSH,1,0)); //Dead Bush
        items.add(new ItemStack(MetaBlocks.plants_nocollisionbiome_1,1,1));//Desert Shrub
        items.add(new ItemStack(MetaBlocks.plants_nocollision_2,1,4));//Dead bushes & Grass
        items.add(new ItemStack(MetaBlocks.plants_nocollision_5,1,1)); //Tumbleweed


        //  Moss and Lillypads  //
        items.add(new ItemStack(Blocks.WATERLILY,1,0)); //Lily Pad
        items.add(new ItemStack(MetaBlocks.plants_lilypad_1,1,0));//Swamp Lilypads
        items.add(new ItemStack(MetaBlocks.plants_lilypad_2,1,0));
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,2));//Moss on the Ground
        items.add(new ItemStack(MetaBlocks.plants_nocollisionconnectedxz_1,1,0));//Lichen

        //  Mushrooms  //
        items.add(new ItemStack(Blocks.BROWN_MUSHROOM,1,0)); //Mushroom
        items.add(new ItemStack(Blocks.RED_MUSHROOM,1,0)); //Mushroom
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,3));//Brown Mushrooms
        items.add(new ItemStack(MetaBlocks.plants_nocollision_3,1,4));//Red Mushrooms
        items.add(new ItemStack(Blocks.BROWN_MUSHROOM_BLOCK,1,0)); //Brown Mushroom Block
        items.add(new ItemStack(Blocks.RED_MUSHROOM_BLOCK,1,0)); //Red Mushroom Block


        //  Nests  //
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,3));//Bird Nest
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,4));//Small Bird Nest
        items.add(new ItemStack(MetaBlocks.wood_fullpartial_1,1,0));//Beehive
        items.add(new ItemStack(MetaBlocks.wood_fullpartial_1,1,1));//Hornets Nest

        //  Bones  //

        items.add(new ItemStack(Blocks.CHORUS_PLANT,1,0)); //Chorus Plant
        items.add(new ItemStack(Blocks.CHORUS_FLOWER,1,0)); //Chorus Flower
        items.add(new ItemStack(Blocks.BONE_BLOCK,1,0)); //Bone
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,11)); //Bones
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,12)); //Skeletons
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,0));//Bones
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,1));//Hanging Body
        items.add(new ItemStack(Items.SKULL,1,0)); //Skeleton Skull
        items.add(new ItemStack(Items.SKULL,1,2)); //Zombie Skull
        items.add(new ItemStack(Items.SKULL,1,4)); //Creeper Skull
        items.add(new ItemStack(Items.SKULL,1,5)); //Dragon Skull
        items.add(new ItemStack(Blocks.COAL_BLOCK,1,0)); //Coal
        items.add(new ItemStack(MetaBlocks.stone_fulldamagelight6_1,1,0));//Hot Coals

        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,11));//Steam
        items.add(new ItemStack(MetaBlocks.nomaterial_nocollision_1,1,12));//Smoke
        items.add(new ItemStack(MetaBlocks.ice_translucentnocollision_1,1,0));//Clouds
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,9));//Thick Clouds
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,10));//Thin Clouds
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,5));//Cow Patty (WIP)

        //  Coral  //
        items.add(new ItemStack(Blocks.SPONGE,1,0)); //Sponge
        items.add(new ItemStack(Blocks.SPONGE,1,1)); //Wet Sponge
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,13)); //Red Coral
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,14)); //Yellow Coral
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,15)); //Blue Coral

        items.add(new ItemStack(Blocks.WEB,1,0)); //Web

        MetaBlocks.itemblock_cloud_white.addAllVariants(items); // cloud_white opacity 0-15


        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Item.getItemFromBlock(Blocks.SAPLING);
    }

}