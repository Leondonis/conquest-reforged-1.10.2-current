package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class TopographyTab extends CreativeTabs {

    public TopographyTab(int index, String newLabel) {
        // Use the same index as the Vanilla tab we want to replace
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //vanillaTab.displayAllRelevantItems(items);

        //  Stone  //
        items.add(new ItemStack(Blocks.STONE,1,0)); //Stone
        items.add(new ItemStack(Blocks.STONE,1,1)); //Granite
        items.add(new ItemStack(MetaBlocks.stone_full_27,1,3)); //Red Granite
        items.add(new ItemStack(MetaBlocks.stone_full_27,1,4)); //Smooth Red Granite
        items.add(new ItemStack(Blocks.STONE,1,3)); //Diorite
        items.add(new ItemStack(Blocks.STONE,1,5)); //Andesite
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,5)); //Quartzite Grey Stone
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,6)); //Quartzite Pink Stone
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,4)); //Icy Stone
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,5));//Icy Stone (Top)
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,6)); //Mossy Stone
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,7)); //Mossy Stone (Top)
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,2)); //Schist Stone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,9)); //Schist Lichen Stone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,10)); //Schist Mossy Stone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,11)); //Schist Weathered Stone
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,4)); //Green Schist Stone
        items.add(new ItemStack(MetaBlocks.stone_full_27,1,2)); //Serpentinite
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,3)); //Gneiss Stone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,3)); //Shale Stone
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,6)); //Greywacke

        items.add(new ItemStack(MetaBlocks.stone_full_18,1,5)); //Chalk Stone

        items.add(new ItemStack(MetaBlocks.stone_full_2,1,7));//Andesite Natural



        //  Slate  //
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,5)); //Cliff Stone
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,6)); //Coastal Stone
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,8)); //Wet Slate
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,9)); //Full Slate
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,3)); //Natural Basalt


        //  Marble  //
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,0)); //Natural Marble
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,10)); //Uncut Marble
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,2)); //Mossy Whiterock

        //  Granite  //
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,11)); //Gray-Pink Granite
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,8)); //Rough Natural Granite
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,1)); //Weathered Granite
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,5)); //Snowy Granite


        //  Sandstone  //
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,4)); //Rough Natural Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,12)); //Natural Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,4)); //Seaside Sandstone

        items.add(new ItemStack(MetaBlocks.stone_full_18,1,7)); //Red Rough Natural Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,15)); //Red Rough Mossy Natural Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,4)); //Tan Rough Natural Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,6)); //Orange Sandstone Natural Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,13)); //Light Brown Mesa Stone
        items.add(new ItemStack(Blocks.HARDENED_CLAY,1,0)); //Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,0)); //White Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,1)); //Orange Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,4)); //Yellow Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,7)); //Gray Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,12)); //Brown Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,14)); //Red Hardened Clay
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,8)); //Red Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,15)); //Black Hardened Clay

        //  Lava  //
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,14)); //Pahoehoe
        items.add(new ItemStack(Blocks.MAGMA,1,0)); //Magma
        items.add(new ItemStack(Blocks.OBSIDIAN,1,0)); //Obsidian

        //  Crystal  //
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,11));//Calcite
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,12));//Rough Calcite
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,13));//Smooth Calcite

        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,1));//Red Crystal
        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,5));//Light Green Crystal
        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,3));//Green Crystal
        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,2));//Blue Crystal
        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,4));//Purple Crystal


        //  Misc  //
        items.add(new ItemStack(Blocks.END_STONE,1,0)); //Endstone
        items.add(new ItemStack(Blocks.BEDROCK,1,0)); //Bedrock

        //  Rocks  //
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,5));//Stalactite
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,6));//Stalagmite
        items.add(new ItemStack(MetaBlocks.climbingrocks,1,0));//Climbing Rocks
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,7));//Sandstone Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,8));//Granite Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,9));//Limestone Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,3));//Schist Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,5));//Slate Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,10));//Sandstone Rough Natural Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,11));//Greywacke Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,12));//Diorite Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,13));//Dolomite Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,14));//Obsidian Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,15));//Natural Andesite Rock
        items.add(new ItemStack(MetaBlocks.stone_nocollision_4,1,0));//Natural Marble Rock

        //Ores
        items.add(new ItemStack(Blocks.GOLD_ORE,1,0)); //Gold Ore
        items.add(new ItemStack(Blocks.IRON_ORE,1,0)); //Iron Ore
        items.add(new ItemStack(Blocks.COAL_ORE,1,0)); //Coal Ore
        items.add(new ItemStack(Blocks.LAPIS_ORE,1,0)); //Lapis Lazuli Ore
        items.add(new ItemStack(Blocks.DIAMOND_ORE,1,0)); //Diamond Ore
        items.add(new ItemStack(Blocks.REDSTONE_ORE,1,0)); //Redstone Ore
        items.add(new ItemStack(Blocks.EMERALD_ORE,1,0)); //Emerald Ore
        items.add(new ItemStack(Blocks.QUARTZ_ORE,1,0)); //Nether Ore

    //Ground

        //  Sand  //
        items.add(new ItemStack(Blocks.SAND,1,0)); //Sand
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,0)); //Sand and Grass
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,1));//Gravel and Sand
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,2));//Vegetation and Sand
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,3));//Wet Sand
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,4));//Wet Sand and Gravel
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,5));//Vegetation and Wet Sand

        //  Red Sand  //
        items.add(new ItemStack(Blocks.SAND,1,1)); //Red Sand
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,6));//Red Sand and Gravel
        items.add(new ItemStack(MetaBlocks.sand_full_1,1,7));//Vegetation and Red Sand

        //  Clay  //
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,15)); //Dry Light Brown Clay
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,0)); //Dry Brown Clay
        items.add(new ItemStack(Blocks.CLAY,1,0)); //Clay

        //  Dirt  //
        items.add(new ItemStack(MetaBlocks.grass_full_1,1,0)); //Grass Stone
        items.add(new ItemStack(Blocks.GRASS,1,0)); //Grass
        items.add(new ItemStack(MetaBlocks.grass_full_1,1,1)); //Forest Grass
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,13));//Thick Moss
        items.add(new ItemStack(Blocks.MYCELIUM,1,0)); //Mycelium
        items.add(new ItemStack(Blocks.DIRT,1,2)); //Podzol
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,10));//Taiga Forest Floor
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,2));//Forest Floor Brown Fir
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,11));//Zautum
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,12));//Overgrown Podzol
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,13));//Old Podzol
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,6));//Podzol Old Vibrant
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,3));//Lorein Podzol
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,4));//Mossy Lorein Podzol
        items.add(new ItemStack(Blocks.DIRT,1,0)); //Dirt
        items.add(new ItemStack(Blocks.DIRT,1,1)); //Coarse Dirt
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,0));//Dirt Block
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,1));//Dirt Bones
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,2));//Dirt Frozen
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,3));//Dirt Gravel
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,4));//Dirt Mossy
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,12));//Lorein Dirt
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,14));//Dirt Lichen
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,3));//Dirty Mossy Rocks
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,5));//Light Dirt
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,6));//Light Dirt Path
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,10));//Light Dirt Path 1
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,9));//Middle Dirt Path 1
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,7));//Dark Dirt Path
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,8));//Dark Dirt Path 1
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,8));//Burnt Dirt
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,4));//Peat
        items.add(new ItemStack(MetaBlocks.ground_soulsand_1,1,1));//Muddy Dirt
        items.add(new ItemStack(MetaBlocks.ground_soulsand_1,1,0));//Mud
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,7));//Mud
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,9));//Ants
        items.add(new ItemStack(MetaBlocks.ground_directionalfullpartial_1,1,0));//Farmland
        items.add(new ItemStack(MetaBlocks.ground_directionalfullpartial_1,1,1));//Farmland Plowed Diagonally

        //  Gravel  //
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,15)); //Debris Sandstone
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,0)); //Debris Schist
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,1)); //Debris Sandstone Mossy
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,2)); //Debris Schist Mossy
        items.add(new ItemStack(Blocks.GRAVEL,1,0)); //Gravel
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,15));//Grassy Gravel
        items.add(new ItemStack(MetaBlocks.ground_full_1,1,14));//Small Stones
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,0));//Grassy Small Stones
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,5));//Soft Gravel
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,1));//White Gravel
        items.add(new ItemStack(MetaBlocks.ground_full_2,1,11));//Smooth Colored Pebbles

        items.add(new ItemStack(Blocks.NETHERRACK,1,0)); //Netherrack
        items.add(new ItemStack(Blocks.SOUL_SAND,1,0)); //Soul Sand
        items.add(new ItemStack(MetaBlocks.sand_layer_2,1,1));//Ash

        //  Ice  //
        items.add(new ItemStack(Blocks.ICE,1,0)); //Ice
        items.add(new ItemStack(Blocks.PACKED_ICE,1,0)); //Packed Ice
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,5)); //Dirty Ice
        items.add(new ItemStack(MetaBlocks.ground_full_3,1,6)); //Glacier Ice
        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,0)); //Normal Ice
        items.add(new ItemStack(MetaBlocks.ice_translucent_1,1,6));//Crystallized Ice
        items.add(new ItemStack(Blocks.SNOW,1,0)); //Snow
        items.add(new ItemStack(Blocks.SNOW_LAYER,1,0)); //Snow Layer
        items.add(new ItemStack(MetaBlocks.snow_stairs_1,1,0));//Snow Stairs
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,0));//Icicles
        items.add(new ItemStack(MetaBlocks.ice_nocollisiondamage_1,1,0));//Ice Spikes
        items.add(new ItemStack(MetaBlocks.ice_lilypad_1,1,0));//Floating Ice

        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Item.getItemFromBlock(Blocks.GRASS);
    }

}