package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class AdvancedRefinedTab extends CreativeTabs {

    public AdvancedRefinedTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //vanillaTab.displayAllRelevantItems(items);

        // Cobblestone Textures //
        items.add(new ItemStack(Blocks.LAPIS_BLOCK,1,0)); //Lapis Lazuli Block
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,13)); //Blue Marble (Stone Brick Corner)

        items.add(new ItemStack(MetaBlocks.stone_directionalfullpartial_1,1,0)); //Stone Gargoyle (?)



        //  Stone Details  //
        items.add(new ItemStack(Blocks.STONE,1,2)); //Polished Granite
        items.add(new ItemStack(Blocks.STONE,1,4)); //Polished Diorite
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,14)); //Smooth Andesite
        items.add(new ItemStack(Blocks.STONE,1,6)); //Polished Andesite
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,8)); //Portcullis Groove
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,9)); //Chiseled Stone
        items.add(new ItemStack(Blocks.STONEBRICK,1,3)); //Black Stone Pillar (Chiseled Stone Brick)
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,10)); //Stone Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,6)); //Gothic Sandstone Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,11)); //Dark Stone Decorative Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,14)); //Dwarven Schist Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,0)); //Dwarven Schist Pillar 1
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,13)); //Dwarven Schist Runes
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,15)); //Dwarven Schist Design 1
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,1)); //Dwarven Schist Design


        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,10));//Carved Stone Window
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,11));//Stone Window
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,2));//Stone Arch Window

        items.add(new ItemStack(Blocks.DROPPER,1,0)); //Dropper
        //Jungle Dispenser
        items.add(new ItemStack(Blocks.DISPENSER,1,0)); //Carved Stone head Dispenser
        items.add(new ItemStack(Items.SKULL,1,1)); //Carved Stone Head

        //  Runes  //
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,6)); //Germanic Glyphs
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,7)); //Germanic Glyphs 2

        //  Iron  //
        items.add(new ItemStack(MetaBlocks.iron_full_1,1,0));//Dark Iron Block
        items.add(new ItemStack(MetaBlocks.iron_stairs_2,1,0));//Metal Grate Stairs
        items.add(new ItemStack(MetaBlocks.iron_railing,1,0));//Metal Railing
        items.add(new ItemStack(MetaBlocks.iron_railing_straight,1,0));//Straight Metal Railing
        items.add(new ItemStack(MetaBlocks.iron_trapdoormodel_1,1,0));//Metal Grate
        items.add(new ItemStack(MetaBlocks.iron_trapdoormodel_2,1,1));//Iron Grate
        items.add(new ItemStack(Blocks.IRON_BLOCK,1,0)); //Iron Block
        items.add(new ItemStack(MetaBlocks.iron_full_1,1,1));//Iron Block 2
        items.add(new ItemStack(MetaBlocks.iron_full_1,1,2));//Iron Block 3
        items.add(new ItemStack(MetaBlocks.iron_full_1,1,3));//Rusty Iron Block
        items.add(new ItemStack(Items.IRON_DOOR,1,0)); //Iron Door
        items.add(new ItemStack(Blocks.IRON_TRAPDOOR,1,0)); //Iron Trapdoor
        items.add(new ItemStack(MetaBlocks.dispenser_iron,1,0));//Iron Dispenser
        items.add(new ItemStack(MetaBlocks.dropper_iron,1,0));//Iron Dropper
        items.add(new ItemStack(Blocks.END_PORTAL_FRAME,1,0)); //End Frame


        items.add(new ItemStack(Blocks.PURPUR_BLOCK,1,0)); //Purpur
        items.add(new ItemStack(Blocks.PURPUR_PILLAR,1,0)); //Purpur Column
        items.add(new ItemStack(Blocks.END_BRICKS,1,0)); //End Bricks

        //  Tudor and Dark Tudor Frames  //
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,1)); //Tudor Slash
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,2)); //Tudor Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,3)); //Tudor X
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,4)); //Tudor Up
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,5)); //Tudor Down
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,6)); //Tudor Box

        items.add(new ItemStack(MetaBlocks.stone_full_3,1,7)); //Tudor Slash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,8)); //Tudor Backslash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,9)); //Tudor Box (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,8)); //Tudor Narrow / Frame
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,9)); //Tudor Narrow \ Frame
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,4)); //Tudor Fancy Frame
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,5)); //Tudor Fancy Frame 1

        items.add(new ItemStack(MetaBlocks.stone_full_23,1,15)); //Tudor Gray Slash
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,0)); //Tudor Gray Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,1)); //Tudor Gray X
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,2)); //Tudor Gray Up
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,8)); //Tudor Gray Down

        items.add(new ItemStack(MetaBlocks.stone_full_23,1,3)); //Tudor Plaster Slash
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,4)); //Tudor Plaster Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,2)); //Tudor Plaster X
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,6)); //Tudor Plaster Up
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,5)); //Tudor Plaster Down
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,10)); //Tudor Plaster Box

        items.add(new ItemStack(MetaBlocks.stone_full_21,1,9)); //Tudor Blue Slash
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,10)); //Tudor Blue Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,11)); //Tudor Blue X
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,12)); //Tudor Blue Up
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,13)); //Tudor Blue Down
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,14)); //Tudor Blue Box

        items.add(new ItemStack(MetaBlocks.stone_full_22,1,5)); //Tudor Red Slash
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,6)); //Tudor Red Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,7)); //Tudor Red X
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,8)); //Tudor Red Up
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,9)); //Tudor Red Down
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,10)); //Tudor Red Box

        items.add(new ItemStack(MetaBlocks.stone_full_22,1,0)); //Tudor Mud Slash
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,1)); //Tudor Mud Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,15)); //Tudor Mud X
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,3)); //Tudor Mud Up
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,4)); //Tudor Mud Down
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,2)); //Tudor Mud Box

        items.add(new ItemStack(MetaBlocks.stone_full_22,1,12)); //Tudor Wattle Slash
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,13)); //Tudor Wattle Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,11)); //Tudor Wattle X
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,15)); //Tudor Wattle Up
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,0)); //Tudor Wattle Down
        items.add(new ItemStack(MetaBlocks.stone_full_22,1,14)); //Tudor Wattle Box

        items.add(new ItemStack(MetaBlocks.stone_full_3,1,10)); //Dark Tudor Slash
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,11)); //Dark Tudor Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,12)); //Dark Tudor X
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,13)); //Dark Tudor Up
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,14)); //Dark Tudor Down
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,15)); //Dark Tudor Box

        items.add(new ItemStack(MetaBlocks.stone_full_4,1,0)); //Dark Tudor Slash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,1)); //Dark Tudor Backslash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,2)); //Dark Tudor Box (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,10)); //Tudor Dark Narrow / Frame
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,11)); //Tudor Dark Narrow \ Frame


        //  Brick, Red Brick and Dark Brick Frames  //
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,3)); //Brick Slash
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,4)); //Brick Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,5)); //Brick X
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,6)); //Brick Up
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,7)); //Brick Down
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,8)); //Brick Box

        items.add(new ItemStack(MetaBlocks.stone_full_4,1,9)); //Brick Slash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,10)); //Brick Backslash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,11)); //Brick Box (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,14)); //Tudor Dark Brick Narrow / Frame
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,15)); //Tudor Dark Brick Narrow \ Frame


        items.add(new ItemStack(MetaBlocks.stone_full_4,1,12)); //Dark Brick Slash
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,13)); //Dark Brick Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,14)); //Dark Brick X
        items.add(new ItemStack(MetaBlocks.stone_full_4,1,15)); //Dark Brick Up
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,0)); //Dark Brick Down
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,1)); //Dark Brick Box

        items.add(new ItemStack(MetaBlocks.stone_full_5,1,2)); //Dark Brick Slash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,3)); //Dark Brick Backslash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,4)); //Dark Brick Box (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,0)); //Tudor Red Brick Narrow / Frame
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,1)); //Tudor Red Brick Narrow \ Frame


        items.add(new ItemStack(MetaBlocks.stone_full_5,1,6)); //Red Brick Slash
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,7)); //Red Brick Backslash
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,5)); //Red Brick X
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,8)); //Red Brick Up
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,9)); //Red Brick Down
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,10)); //Red Brick Box

        items.add(new ItemStack(MetaBlocks.stone_full_5,1,11)); //Red Brick Slash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,12)); //Red Brick Backslash (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,13)); //Red Brick Box (CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,12)); //Tudor Brick Narrow / Frame
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,13)); //Tudor Brick Narrow \ Frame


        //Netherbrick and Red Tiles
        items.add(new ItemStack(Blocks.NETHER_BRICK,1,0)); //Netherbrick
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,11)); //Normal Netherbrick
        items.add(new ItemStack(Blocks.RED_NETHER_BRICK,1,0)); //Red Nether Brick
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,12)); //Big Netherbrick
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,13)); //Netherbrick Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,14)); //Carved Netherbrick
        items.add(new ItemStack(Blocks.NETHER_WART_BLOCK,1,0)); //Nether Wart Block
        items.add(new ItemStack(MetaBlocks.stone_full_3,1,0)); //Red Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,2)); //Arnor Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,7)); //Gray Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,4)); //Engraved Elven Design
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,12)); //Engraved Elven Design 1

        items.add(new ItemStack(Blocks.RED_SANDSTONE,1,1)); //Chiseled Red Sandstone
        items.add(new ItemStack(Blocks.RED_SANDSTONE,1,2)); //Smooth Red Sandstone

        //  Sandstone //
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,8)); //Blue Marble (Sandstone Corners)
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,9)); //Blue Marble (Sandstone Corners) 2
        items.add(new ItemStack(MetaBlocks.stone_directionalfullpartial_1,1,1)); //Sandstone Gargoyle (?)

        items.add(new ItemStack(MetaBlocks.stone_full_6,1,10)); //Chiseled Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,13)); //Frieze Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,14)); //Inscribed Sandstone
        items.add(new ItemStack(Blocks.SANDSTONE,1,1)); //Sandstone Chiseled
        items.add(new ItemStack(MetaBlocks.dispenser_egyptian,1,0));//Sandstone Dispenser
        items.add(new ItemStack(MetaBlocks.dropper_sandstone,1,0));//Sandstone Dropper

        //  Sandstone and Marble Columns  //
        items.add(new ItemStack(Blocks.SANDSTONE,1,2)); //Sandstone Smooth Column

        items.add(new ItemStack(MetaBlocks.stone_log_1,1,0)); //Sandstone Column
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,0)); //Capital Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,1)); //Base Sandstone

        items.add(new ItemStack(MetaBlocks.stone_log_1,1,1)); //Red Column

        items.add(new ItemStack(MetaBlocks.stone_full_7,1,2)); //Capital Red Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,3)); //Base Red Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,4)); //Capital Red Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,5)); //Base Red Sandstone

        items.add(new ItemStack(MetaBlocks.stone_log_1,1,2)); //Blue Column
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,6)); //Capital Blue Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,7)); //Base Blue Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,8)); //Capital Blue Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,9)); //Base Blue Sandstone

        items.add(new ItemStack(MetaBlocks.stone_log_1,1,3)); //Gold Column
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,10)); //Capital Gold Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,11)); //Base Gold Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,12)); //Capital Gold Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,13)); //Base Gold Sandstone

        items.add(new ItemStack(MetaBlocks.stone_log_2,1,0)); //Marble Column
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,14)); //Capital Marble
        items.add(new ItemStack(MetaBlocks.stone_full_7,1,15)); //Base Marble

        items.add(new ItemStack(MetaBlocks.stone_log_2,1,1)); //Elven Clean Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,10)); //Elven Decorative Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,12)); //Elven Wave Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,13)); //Gray Ivory Pillar
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,14)); //Forlond Pillar
        items.add(new ItemStack(MetaBlocks.stone_log_2,1,2)); //Elven Pillar

        items.add(new ItemStack(MetaBlocks.stone_log_2,1,3)); //Capital Ionian Marble
        items.add(new ItemStack(MetaBlocks.stone_log_3,1,0)); //Capital Ionian Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,0)); //Capital Corinthian
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,1)); //Capital Corinthian Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,6)); //Capital Corinthian Polychrome


        items.add(new ItemStack(MetaBlocks.stone_full_8,1,2)); //Cornice Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,3)); //Architrave Polychrome
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,4)); //Architrave Polychrome Legionares
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,5)); //Architrave Polychrome 2

        items.add(new ItemStack(MetaBlocks.stone_full_8,1,3)); //Plinth Sandstone

        items.add(new ItemStack(MetaBlocks.stone_full_8,1,4)); //Marble and Sandstone Base
        items.add(new ItemStack(MetaBlocks.base_marblesandstone_endframe,1,0)); //Marble and Sandstone EndFrame

        items.add(new ItemStack(MetaBlocks.stone_full_8,1,5)); //Cornice Marble

        items.add(new ItemStack(MetaBlocks.stone_full_8,1,6)); //Marble Base
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,14)); //Marble Pedestal
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,7)); //Marble Base Design
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,8)); //Marble Base Design 2


        items.add(new ItemStack(MetaBlocks.stone_full_21,1,5)); //Smooth Marble Column
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,2)); //Smooth Marble Column Base
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,9)); //Smooth Marble Column Capital


        items.add(new ItemStack(Blocks.QUARTZ_BLOCK,1,1)); //Parian Marble Frieze
        items.add(new ItemStack(Blocks.QUARTZ_BLOCK,1,2)); //Parian Marble Column
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,9)); //Base Parian Marble
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,10)); //Capital Parian Marble
        items.add(new ItemStack(MetaBlocks.stone_fence_1,1,6));//Marble Railing
        items.add(new ItemStack(MetaBlocks.wood_trapdoormodel_50,1,0));//White Trellis


        //Letters
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,11)); //Engraved A
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,12)); //Engraved B
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,13)); //Engraved C
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,14)); //Engraved D
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,15)); //Engraved E
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,0)); //Engraved F
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,1)); //Engraved G
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,2)); //Engraved H
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,3)); //Engraved I
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,4)); //Engraved Interpunct
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,5)); //Engraved K
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,6)); //Engraved L
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,7)); //Engraved M
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,8)); //Engraved M
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,9)); //Engraved O
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,10)); //Engraved P
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,11)); //Engraved Q
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,12)); //Engraved R
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,13)); //Engraved S
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,14)); //Engraved T
        items.add(new ItemStack(MetaBlocks.stone_full_9,1,15)); //Engraved V

        //Mosaics
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,10)); //Roman Mosiac
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,11)); //Indian Mosiac
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,12)); //Andalusian Mosaic
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,7)); //Black and White Marble
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,8)); //Small Diagonal Checkered Marble
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,9)); //Small Checkered Marble
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,10)); //Red and White Marble
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,11)); //Red and White Marble 2
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,12)); //Blue and Yellow Marble
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,13)); //Diagonal Checkered Marble
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,14)); //Checkered Marble

        //Stucco and Wall Designs
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,7)); //Dark Brown Worn Stucco

        items.add(new ItemStack(MetaBlocks.stone_full_23,1,1)); //Peach Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,6)); //Peach Worn Stucco

        items.add(new ItemStack(MetaBlocks.stone_full_10,1,13)); //(Yellow) Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,1)); //Yellow Clean Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,9)); //Yellow Cobblestone Worn Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,14)); //White Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,2)); //White Clean Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,7)); //PLaster Worn Red lines
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,14)); //Plaster Red lines
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,15)); //Plaster Masonry Lines

        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,2)); //Magenta Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,15)); //Magenta Clean Stucco

        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,8)); //Light Gray Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,0)); //Light Gray Clean Stucco

        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,10)); //Purple Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,3)); //Purple Clean Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,4)); //Orange Stone
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,2)); //Orange Harad Wall


        items.add(new ItemStack(MetaBlocks.stone_full_17,1,7)); //Blue Wallpaper
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,8)); //Clean Blue Wallpaper
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,9)); //Green Wallpaper
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,10)); //Clean Green Wallpaper
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,11)); //Red Wallpaper
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,12)); //Clean Red Wallpaper
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,14)); //Light Blue Mithlond Wall
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,0)); //Fancy Gray Mithlond Wall


        items.add(new ItemStack(MetaBlocks.stone_full_11,1,5)); //Decorative Mosaic
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,6)); //White Wall Design
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,7)); //White Wall Design 2
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,8)); //White Wall Design 3
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,9)); //White Wall Design 4
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,10)); //White Wall Design 5
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,11)); //White Wall Design 6

        items.add(new ItemStack(MetaBlocks.stone_full_11,1,12)); //White Stucco Wall
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,13)); //Tan Stucco Wall
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,14)); //Brown Wall Design
        items.add(new ItemStack(MetaBlocks.stone_full_11,1,15)); //Brown Stucco Wall
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,0)); //Purple Stucco Wall
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,1)); //Magenta Wall Design
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,2)); //Magenta Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,3)); //Red Wall Design
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,4)); //Red Wall Design 2
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,5)); //Red Wall Design 3
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,6)); //Red Wall Design 4
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,7)); //Red Wall Design 5
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,8)); //Red Wall Design 6
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,9)); //Red Wall Design 7
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,10)); //Red Wall Design 8
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,11)); //Light Red Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,12)); //Orange Wall Design
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,13)); //Orange Wall Design 2
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,14)); //Lime Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_12,1,15)); //Green Wall Design
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,0)); //Green Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,1)); //Cyan Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,2)); //Light Blue Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,3)); //Blue Stucco
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,4)); //Black Stucco

        items.add(new ItemStack(MetaBlocks.stone_full_25,1,6)); //Sindarin Wall
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,7)); //Sindarin Wall 1
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,8)); //Sindarin Wall 2
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,9)); //Sindarin Wall 3
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,10)); //Sindarin Wall 4
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,11)); //Sindarin Wall 5
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,9)); //White Plaster Design
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,10)); //White Plaster Design 2

        //More Roman Stuff
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,5)); //Roman Ornate Door
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,6)); //Roman Ornate Door 2
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,0)); //Bronze Lattice
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,7)); //Gilded Bronze Block
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,8)); //Fancy Bronze Block
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,12)); //Fancy Bronze Block (Top on Sides)
        items.add(new ItemStack(MetaBlocks.stone_full_13,1,9)); //Gold Bars
        items.add(new ItemStack(Blocks.GOLD_BLOCK,1,0)); //Gold Block
        items.add(new ItemStack(Blocks.EMERALD_BLOCK,1,0)); //Emerald Block
        items.add(new ItemStack(Blocks.REDSTONE_BLOCK,1,0)); //Redstone Block

        //Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,0)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,1)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,2)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,3)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,4)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,5)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,6)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,7)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,8)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,9)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,10)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,11)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,12)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,13)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,14)); //Stained Glass
        items.add(new ItemStack(Blocks.STAINED_GLASS,1,15)); //Stained Glass
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,1));//Sindar Window
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,3));//Noldor Window
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,4));//Hobbit Window 1
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,5));//Hobbit Window 2
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,6));//Hobbit Window 3
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,7));//Hobbit Window 4
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,0));//Hobbit Window 5
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,1));//Hobbit Window 6
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,2));//Hobbit Window 7
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,3));//Hobbit Window 8
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,4));//Yellow Hobbit Window 1
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,5));//Yellow Hobbit Window 2
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,6));//Yellow Hobbit Window 3
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,7));//Yellow Hobbit Window 4
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,8));//Yellow Hobbit Window 5
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,9));//Yellow Hobbit Window 6
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,10));//Yellow Hobbit Window 7
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,11));//Yellow Hobbit Window 8
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,12));//Large Hobbit Window 1
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,13));//Large Hobbit Window 2
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,14));//Large Hobbit Window 3
        items.add(new ItemStack(MetaBlocks.glass_glass_3,1,15));//Large Hobbit Window 4

        items.add(new ItemStack(Blocks.GLASS,1,0)); //Glass
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,0));//Glass Block (No Connected Textures)
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,1));//Straight Glass Block
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,3));//Fancy Glass Block
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,5));//Dragon Glass Block
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,7));//Window
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,8));//Brown Window(CTM Elongated)
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,9));//Brown Window (CTM Round Top)
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,6));//Brown Window Round
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,10));//Yellow window
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,11));//Yellow Window (CTM Round Top)
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,12));//Yellow Window (CTM Elongated)
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,7));//Yellow Window Round
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,13));//Green Window
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,14));//Green Window (CTM Round Top)
        items.add(new ItemStack(MetaBlocks.glass_glass_1,1,15));//Green Window (CTM Elongated)
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,8));//Green Window Round
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,0));//White Window
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,1));//White Window (CTM Round Top)
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,2));//White Window (CTM Elongated)
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,9));//White Window Round
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,5));//Asian Window
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,3));//Shoji
        items.add(new ItemStack(MetaBlocks.stone_verticalslab_1,1,3));//Bay Window
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,12));//Wooden Pane
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,0));//Wooden Pane 1
        items.add(new ItemStack(MetaBlocks.glass_glass_4,1,8));//Wooden Pane 2

    //WOOD
        items.add(new ItemStack(Blocks.HOPPER,1,0)); //Hopper
        items.add(new ItemStack(MetaBlocks.wood_full_9,1,0));//Oak Log Design Block
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,6));//Carved Oak Wood Block
        items.add(new ItemStack(Items.OAK_DOOR,1,0)); //Oak Door
        items.add(new ItemStack(MetaBlocks.door_oaknowindow,1,0)); //Oak No Window Door
        items.add(new ItemStack(MetaBlocks.door_wood2,1,0)); //Reinforced Wooden Gate
        items.add(new ItemStack(MetaBlocks.door_wood1,1,0)); //Wood Door
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,9));//Carved Taiga Wood Block
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,6));//Carved Taiga Horizontal Pattern
        items.add(new ItemStack(Items.SPRUCE_DOOR,1,0)); //Spruce Door
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,10));//Carved Birch Wood Block
        items.add(new ItemStack(Items.BIRCH_DOOR,1,0)); //Birch Door
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,11));//Carved jungle Wood Block
        items.add(new ItemStack(Items.JUNGLE_DOOR,1,0)); //Jungle Door
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,13));//Carved Acacia Wood Block
        items.add(new ItemStack(Items.ACACIA_DOOR,1,0)); //Acacia Door
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,14));//Carved Dark Oak Wood Block
        items.add(new ItemStack(Items.DARK_OAK_DOOR,1,0)); //Dark Oak Door
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,9));//Birch Carved Gilded Design
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,10));//Wood Carved Gilded Design
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,11));//Oak Carved Gilded Design

        items.add(new ItemStack(MetaBlocks.wood_full_8,1,2)); //Sindar Beam
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,12)); //Dark Carved Beam
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,1)); //Wood Panel
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,8)); //Elven Wall
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,9)); //Elven Wall 1
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,10)); //Elven Wall 2
        items.add(new ItemStack(MetaBlocks.door_elven1,1,0)); //Elven Door 1
        items.add(new ItemStack(MetaBlocks.door_elven2,1,0)); //Elven Door 2

        items.add(new ItemStack(MetaBlocks.wood_log_2,1,2));//Ornamental Log
        items.add(new ItemStack(MetaBlocks.wood_trapdoormodel_61,1,1));//Wood Lattice Trapdoor
        items.add(new ItemStack(MetaBlocks.shutters_light,1,0));//Light Shutters
        items.add(new ItemStack(MetaBlocks.trapdoor_lightslotted,1,0));//Light Slotted Shutters
        items.add(new ItemStack(MetaBlocks.wood_log_2,1,3));//Ornamental Dark Log
        items.add(new ItemStack(MetaBlocks.shutters_dark,1,0));//Dark Shutters
        items.add(new ItemStack(MetaBlocks.trapdoor_darkslotted,1,0));//Dark Slotted Shutters


        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,7));//Birch Shutters
        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,8));//Oak Shutters
        items.add(new ItemStack(MetaBlocks.wood_fencegate_13,1,1));//Wooden Shutters
        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,9));//White Shutters
        items.add(new ItemStack(MetaBlocks.shutters_white,1,0));//White Shutters
        items.add(new ItemStack(MetaBlocks.shutters_green,1,0));//Green Shutters
        items.add(new ItemStack(MetaBlocks.shutters_blue,1,0));//Blue Shutters
        items.add(new ItemStack(MetaBlocks.shutters_yellow,1,0));//Yellow Shutters
        items.add(new ItemStack(MetaBlocks.shutters_orange,1,0));//Orange Shutters
        items.add(new ItemStack(MetaBlocks.shutters_red,1,0));//Red Shutters

        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,2));//Painted Wooden Fence
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,5));//Gray Painted Horizontal Planks
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,4));//White Painted Horizontal Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,1));//White Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,2));//White Painted Wood
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,3));//Red Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,4));//Red Painted Wood
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,5));//Light Red Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,6));//Weathered Light Red Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,7));//Orange Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,8));//Yellow Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,9));//Yellow Painted Wood
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,10));//Green Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,11));//Green Painted Wood
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,12));//Lime Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,13));//Weathered Lime Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,14));//Cyan Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,15));//Weathered Cyan Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,0));//Blue Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,1));//Weathered Dark Blue Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,2));//Blue Painted Wood
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,3));//Light Blue Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,4));//Weathered Light Blue Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,5));//Purple Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,6));//Brown Painted Planks
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,7));//Weathered Brown Planks

        items.add(new ItemStack(MetaBlocks.wood_log_3,1,0));//Rope around a Log
        items.add(new ItemStack(MetaBlocks.wood_log_3,1,1));//Chains around a Log

        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Item.getItemFromBlock(MetaBlocks.stone_log_1);
    }
}