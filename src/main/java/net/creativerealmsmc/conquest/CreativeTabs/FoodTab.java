package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class FoodTab extends CreativeTabs {

    public FoodTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //  Containers  //
        items.add(new ItemStack(Blocks.MELON_BLOCK,1,0)); //Melon Block
        items.add(new ItemStack(MetaBlocks.box_wheat,1,0));//Box of Wheat
        items.add(new ItemStack(MetaBlocks.box_grapes,1,0));//Box of Grapes
        items.add(new ItemStack(MetaBlocks.box_fruit,1,0));//Box of Fruit
        items.add(new ItemStack(MetaBlocks.box_carrots,1,0));//Box of Carrots
        items.add(new ItemStack(MetaBlocks.box_beetroot,1,0));//Box of Beetroots
        items.add(new ItemStack(MetaBlocks.box_apple,1,0));//Box of Apples
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,6));//Apple
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,8));//Barrel of Apples
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,9));//Sack of Apples
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,10));//Wicker Basket of Apples
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,7)); //Barrel of Eggs
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,11));//Barrel of Bread
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,12));//Sack of Bread
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,13));//Sack of Flour
        items.add(new ItemStack(MetaBlocks.wood_full_4,1,14));//Barrel of Cabbages
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,0));//Barrel of Fish
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,1));//Sack of Fish
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,4));//Barrel of Cocoa
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,3));//Sack of Cocoa
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,2));//Wicker Basket of Cocoa
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,5));//Barrel of Potatoes
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,6));//Sack of Potatoes
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,7));//Sack of Hops
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,5));//Barrel Turnips
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,8));//Sack of Grapes
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,9));//Wine Keg (Open)
        items.add(new ItemStack(MetaBlocks.wood_log_4,1,2));//Wine Rack
        items.add(new ItemStack(MetaBlocks.wood_log_7,1,1));//Elven Wine Rack

        //  Block Foods (Cakes) //
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,12));//Cut Cheese
        items.add(new ItemStack(MetaBlocks.cake_cheesewheel,1,0));//Cheese Wheel
        items.add(new ItemStack(MetaBlocks.cake_applepie,1,0));//Apple Pie
        items.add(new ItemStack(MetaBlocks.cake_icingfruit,1,0));//Iced Cake with Fruit
        items.add(new ItemStack(MetaBlocks.cake_icingchocolate,1,0));//Chocolate Pound Cake
        items.add(new ItemStack(MetaBlocks.cake_bread,1,0));//Bread
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,7));//Bread Basket
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,11));//Jug of Milk and Bread
        //items.add(new ItemStack(MetaBlocks.stone_nocollision_4,1,1));//Spice Jars
        //items.add(new ItemStack(MetaBlocks.stone_nocollision_4,1,2));//Jam Jars

        //  Hanging Meat //
        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,4));//Pig on a Spit
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,7));//Brown Hanging Rabbit
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,8));//White Hanging Rabbit
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,9));//Hanging Rabbit
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,10));//Beef Cut
        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,5));//Hanging Sausages
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,11));//Big Sausages
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,12));//Little Sausages

        // Hanging Fish //
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,13));//Hanging Fish
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,14));//Hanging Swordfish
        items.add(new ItemStack(MetaBlocks.plants_nocollision_6,1,15));//Hanging Erotic Fish
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,0));//Hanging Clownfish
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,1));//Hanging Sardines

        // Hanging Vegetables //
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,2));//Carrot Bundle
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,3));//Onion Bundle
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,4));//Banana Bundle
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,5));//Herbs
        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,6));//Pipeweed Rack


        // Eating Utensils //
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,12));//Stone Plate
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,2));//Stone Plate of Food
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,4));//Stone Plate of Water
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,13));//Stone Plate Ash
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,10));//Metal Plate
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,2));//Metal Plate of Food
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,11));//Blue Plate
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,0));//Metal Plate of Food
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,14));//Metal Plate of Food
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,3));//Clay Plate of Food
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,15));//Wooden Plate
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,4));//Wooden Plate of Food
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,6));//Wooden Tankard
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,1));//Broken Bottle
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,2));//Bottle
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,3));//Potion Glass
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,4));//Goblet
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,9));//Goblet of Wine
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,5));//Tall Empty Wine Glass
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,6));//Tall Full Wine Glass
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,7));//Short Empty Wine Glass
        items.add(new ItemStack(MetaBlocks.ice_nocollision_1,1,8));//Short Empty Wine Glass



        CreativeTabs.FOOD.displayAllRelevantItems(items);

        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Items.APPLE;
    }


}