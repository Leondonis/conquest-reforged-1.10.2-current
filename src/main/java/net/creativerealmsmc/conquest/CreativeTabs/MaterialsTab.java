package net.creativerealmsmc.conquest.CreativeTabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class MaterialsTab extends CreativeTabs {

    public MaterialsTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        CreativeTabs.MATERIALS.displayAllRelevantItems(items);
        CreativeTabs.BREWING.displayAllRelevantItems(items);

        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Items.STICK;
    }


}