package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class SimpleRefinedTab extends CreativeTabs {

    public SimpleRefinedTab(int index, String newLabel) {
        // Use the same index as the Vanilla tab we want to replace
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //vanillaTab.displayAllRelevantItems(items);

        items.add(new ItemStack(MetaBlocks.stone_full_1,1,0)); //Plastered Stone
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,1)); //Plastered Slate
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,4)); //Hewn Stone Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,5)); //Colorful Slate
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,14)); //Light Mortar Slate
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,2)); //Hewn Stone
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,3)); //Hewn Stone with Stone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,3)); //Dark Cobble


        items.add(new ItemStack(Blocks.COBBLESTONE,1,0)); //Cobblestone
        // Cobblestone Textures //

        items.add(new ItemStack(MetaBlocks.stone_full_1,1,8)); //Old Cobblestone
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,15)); //Drystone Cobblestone

        items.add(new ItemStack(MetaBlocks.stone_full_1,1,10)); //Fishscale Cobblestone
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,11)); //Dirty Fishscale Cobblestone
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,3)); //Colorful Fishscale Cobblestone

        items.add(new ItemStack(MetaBlocks.stone_full_1,1,12)); //Brick Corner Cobblestone
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,9)); //Frozen Cobblestone
        items.add(new ItemStack(Blocks.MOSSY_COBBLESTONE,1,0)); //Cobblestone Mossy
        items.add(new ItemStack(MetaBlocks.stone_full_1,1,15)); //Mossy Cobblestone (All Sides)
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,0)); //Overgrown Cobblestone
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,1)); //Cobblestone with Vines

        //  Stone Brick  //
        items.add(new ItemStack(Blocks.STONEBRICK,1,0)); //Stone Brick
        items.add(new ItemStack(Blocks.STONEBRICK,1,1)); //Stone Brick Mossy
        items.add(new ItemStack(Blocks.STONEBRICK,1,2)); //Stone Brick Cracked

        items.add(new ItemStack(MetaBlocks.stone_full_2,1,2)); //Overgrown Stone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_26,1,14)); //Ruined Stone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,3)); //Chiseled Stone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,4)); //Stone Cobblestone
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,13)); //Dark Mudstone Stonebrick

        //  Bricks  //
        items.add(new ItemStack(Blocks.BRICK_BLOCK,1,0)); //Brick
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,14)); //Brick
        items.add(new ItemStack(MetaBlocks.stone_full_5,1,15)); //Mossy Brick

        items.add(new ItemStack(MetaBlocks.stone_full_6,1,0)); //Dark Red Brick

        items.add(new ItemStack(MetaBlocks.stone_full_6,1,1)); //Mossy Dark Red Brick

        items.add(new ItemStack(MetaBlocks.stone_full_6,1,2)); //Red Brick

        items.add(new ItemStack(MetaBlocks.stone_full_6,1,3)); //Red Mossy Brick
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,11)); //Orange-Red Brick


        //  Roman Brick //
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,4)); //Roman Brick
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,1)); //Roman Vertical Brick
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,7)); //Roman Sandstone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,5)); //Dark Roman Brick
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,6)); //Dark Roman Sandstone Brick

        //  Travertine  //
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,5)); //Diorite Brick
        items.add(new ItemStack(MetaBlocks.stone_full_27,1,0)); //Worn Diorite Brick
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,6)); //Travertine Brick

        items.add(new ItemStack(MetaBlocks.stone_full_23,1,12)); //Light Gray Stone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_23,1,13)); //Dark Gray Stone Brick

        //  Big Sandstone  //
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,7)); //Big Sandstone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,13)); //Dark Cobble Sandstone

        //  Sandstone //
        items.add(new ItemStack(Blocks.RED_SANDSTONE,1,0)); //Red Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,11)); //Conglomerate Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,3)); //Dirty Cobblestone Sandstone
        items.add(new ItemStack(Blocks.SANDSTONE,1,0)); //Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,12)); //Mossy Sandstone
        items.add(new ItemStack(MetaBlocks.stone_full_6,1,15)); //Sandstone Brick
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,1)); //Sandstone Dark




        //  Clay Tile  //
        items.add(new ItemStack(MetaBlocks.stone_full_17,1,15)); //Clay Tile
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,0)); //Mixed Tile
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,1)); //Light Tile

        items.add(new ItemStack(MetaBlocks.stone_full_24,1,12)); //Big Slab Limestone
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,15)); //Cut Limestone
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,7)); //Marble
        items.add(new ItemStack(MetaBlocks.stone_full_8,1,8)); //Smooth White Marble
        items.add(new ItemStack(Blocks.QUARTZ_BLOCK,1,0)); //Block of Quartz
        items.add(new ItemStack(MetaBlocks.stone_full_15,1,15)); //Black Marble
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,1)); //Gray Marble
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,3)); //Pink Marble
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,4)); //Red Marble
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,2)); //Green Marble
        items.add(new ItemStack(MetaBlocks.stone_full_16,1,0)); //Blue Marble

        //  Roman Blocks  //
        items.add(new ItemStack(MetaBlocks.stone_full_27,1,1)); //Marble Slabs
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,0)); //Marble Big Slabs
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,1)); //Marble Big Slabs (No Side CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,2)); //Large Sandstone Slabs
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,3)); //Large Sandstone Slabs (No Side CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,4)); //Large Inscribed Sandstone Slabs
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,5)); //Large Inscribed Sandstone Slabs (No Side CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_24,1,11)); //Large Dark Sandstone Slabs

        items.add(new ItemStack(MetaBlocks.stone_full_10,1,6)); //Large Black Slabs
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,7)); //Large Black Slabs (No Side CTM)
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,8)); //Large Black Inscribed Slabs
        items.add(new ItemStack(MetaBlocks.stone_full_10,1,9)); //Large Black Inscribed Slabs (No Side CTM)

        items.add(new ItemStack(Blocks.PRISMARINE,1,0)); //Prismarine
        items.add(new ItemStack(Blocks.PRISMARINE,1,1)); //Prismarine Bricks
        items.add(new ItemStack(Blocks.PRISMARINE,1,2)); //Dark Prismarine
        items.add(new ItemStack(MetaBlocks.stone_full_25,1,5)); //Sea Shell Wall


        //Roof Tiles
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,3)); //Light Blue Roof Tiles Hardened Clay
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,13)); //Slate Light Blue Roof Tiles
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,11)); //Blue Roof Tiles Hardened Clay
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,2)); //Wavy Blue Roof Tiles
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,5)); //Lime Roof Tiles Hardened Clay
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,14)); //Slate Light Green Roof Tiles
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,13)); //Green Roof Tiles Hardened Clay
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,6)); //Pink Roof Tiles Hardened Clay
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,0)); //Red Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,15)); //Slate Light Red Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,0)); //Slate Red Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,1)); //Slate Reddish Brown Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,3)); //Wavy Red Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,6)); //Reddish Brown Mossy Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_2,1,5)); //Roman Roof Tiles
        items.add(new ItemStack(Blocks.STAINED_HARDENED_CLAY,1,9)); //Cyan Roof Tiles Hardened Clay
        items.add(new ItemStack(MetaBlocks.stone_full_14,1,1)); //Gray Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,4)); //Wavy Gray Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,8)); //Wavy Light Gray Roof Tiles

        items.add(new ItemStack(MetaBlocks.stone_full_14,1,2)); //Brown Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,12)); //Old Wood Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,11)); //Wood Roof Tiles
        items.add(new ItemStack(MetaBlocks.stone_full_21,1,7)); //Wavy Gold Roof Tiles

        items.add(new ItemStack(MetaBlocks.stone_full_14,1,3)); //Oxidized Copper

    //Thatch/Hay
        items.add(new ItemStack(Blocks.HAY_BLOCK,1,0)); //Hay Bale
        items.add(new ItemStack(MetaBlocks.plants_log_1,1,0));//Thatch
        items.add(new ItemStack(MetaBlocks.plants_log_1,1,3));//Thatch Black
        items.add(new ItemStack(MetaBlocks.plants_log_2,1,0));//Thatch Brown
        items.add(new ItemStack(MetaBlocks.plants_log_2,1,1));//Thatch Yellow

        items.add(new ItemStack(MetaBlocks.plants_log_1,1,1));//Gray Thatch
        items.add(new ItemStack(MetaBlocks.plants_log_1,1,2));//Gray Woven Thatch
    //Wood
        items.add(new ItemStack(MetaBlocks.wood_log_9,1,3));//Wood Logs (Beaversticks)
        items.add(new ItemStack(MetaBlocks.wood_log_3,1,2));//Wood Logs (Firewood)
        items.add(new ItemStack(Blocks.PLANKS,1,0)); //Oak Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,1));//Ash Colored Planks
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,13));//Oak Planks 1
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,14));//Reinforced Oak Planks
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,15));//Oak Nails Planks
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,3));//Oak Vertical Planks
        items.add(new ItemStack(MetaBlocks.wood_log_9,1,2));//Oak Beam
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,5));//Oak Platform
        items.add(new ItemStack(MetaBlocks.wood_fence_1,1,7));//Plain Wood Fence
        items.add(new ItemStack(MetaBlocks.wood_fence_1,1,8));//Plain Wood Fence
        items.add(new ItemStack(MetaBlocks.wood_fence_5,1,3));//Plain Wood Fence

        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,0));//Wattle Fence
        items.add(new ItemStack(MetaBlocks.wood_fence_5,1,2));//Wattle
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,8));//Stave
        items.add(new ItemStack(MetaBlocks.woodenboard,1,0));//Wooden Board Trapdoor
        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,1));//Wooden Fence
        items.add(new ItemStack(MetaBlocks.jungle_rail,1,0));//Jungle Rail
        items.add(new ItemStack(Blocks.PLANKS,1,1)); //Spruce Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,0));//Spruce Ironbound Planks
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,7));//Spruce Platform
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,8));//Spruce Planks
        items.add(new ItemStack(MetaBlocks.wood_log_5,1,3));//Spruce Beam
        items.add(new ItemStack(MetaBlocks.wood_anvil_1,1,1));//Wooden Support
        items.add(new ItemStack(Blocks.PLANKS,1,2)); //Birch Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,10)); //Light Plain Birch Planks
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,7));//Pine Smooth Planks
        items.add(new ItemStack(MetaBlocks.wood_log_6,1,0));//Birch Beam
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,3));//Birch Logs
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,4));//Dark Birch Logs
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,12));//Oak Logs
        items.add(new ItemStack(MetaBlocks.wood_full_8,1,15));//Spruce Logs
        items.add(new ItemStack(Blocks.PLANKS,1,3)); //Jungle Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,11)); //Jungle Frame Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_log_6,1,1));//Jungle Beam
        items.add(new ItemStack(Blocks.PLANKS,1,4)); //Acacia Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_log_6,1,2));//Acacia Beam
        items.add(new ItemStack(MetaBlocks.wood_full_2,1,12));//Red Acacia Block

        items.add(new ItemStack(MetaBlocks.wood_ironbar_1,1,3));//Wooden Framing
        items.add(new ItemStack(MetaBlocks.wood_fence_1,1,9));//Newel Caped Wood Fence
        items.add(new ItemStack(Blocks.PLANKS,1,5)); //Dark Oak Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,9)); //Dark Wood Planks
        items.add(new ItemStack(MetaBlocks.wood_log_6,1,3));//Dark Oak Beam

        items.add(new ItemStack(MetaBlocks.wood_full_2,1,15));//Driftwood
        items.add(new ItemStack(MetaBlocks.wood_full_3,1,0));//Mossy Wood


        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Item.getItemFromBlock(MetaBlocks.stone_full_1);
    }
}