package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class MiscellaneousTab extends CreativeTabs {

    public MiscellaneousTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        items.add(new ItemStack(Blocks.BARRIER,1,0)); //Barrier Block
        items.add(new ItemStack(MetaBlocks.glass_pane_2,1,0));//Invisible Glass Pane
        items.add(new ItemStack(MetaBlocks.invisible_light6,1,0));//Low Invisible Light Block
        items.add(new ItemStack(MetaBlocks.invisible_light8,1,0));//Medium Invisible Light Block
        items.add(new ItemStack(MetaBlocks.invisible_light10,1,0));//High Invisible Light Block

        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,3));//Green Screen
        items.add(new ItemStack(MetaBlocks.stone_full_19,1,15));//Planning Approved
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,8));//Planning Upper Class
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,0));//Planning Middle Class
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,9));//Planning Lower Class
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,1));//Planning Name
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,2));//Planning Comment
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,3));//Planning Other
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,4));//Planning Sign
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,5));//Planning Special
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,6));//Planning Waiting Approval
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,7));//Planning Not Approved
        items.add(new ItemStack(MetaBlocks.stone_full_20,1,10));//Planning Update

        items.add(new ItemStack(Blocks.SLIME_BLOCK,1,0)); //Slime
        CreativeTabs.MISC.displayAllRelevantItems(items);
        items.add(new ItemStack(Items.END_CRYSTAL)); //End Crystal

        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Items.LAVA_BUCKET;
    }


}