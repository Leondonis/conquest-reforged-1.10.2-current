package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ToolsTab extends CreativeTabs {

    public ToolsTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items

        //  Tools and Racks  //
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_9,1,0));//Hand Cart
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_1,1,0));//Weapon Rack (Swords)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_1,1,1));//Weapon Rack (Axes)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_1,1,2));//Weapon Rack (Maces)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_4,1,0));//Weapon Rack (Hammers)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_2,1,0));//Weapon Rack (Daggers)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_2,1,1));//Weapon Rack (Shortswords)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_1,1,3));//Weapon Rack (Crossbow)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_2,1,2));//Weapon Rack (Pistols)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_2,1,3));//Weapon Rack (Halberds)
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_3,1,0));//Weapon Rack (Muskets)
        items.add(new ItemStack(MetaBlocks.wood_nocollisiondamage_1,1,0));//Spears
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,3));//Arrow Bundle
        items.add(new ItemStack(MetaBlocks.wood_directionalnocollision_1,1,1));//Arrows
        items.add(new ItemStack(MetaBlocks.wood_directionalnocollision_1,1,0));//Bow
        items.add(new ItemStack(MetaBlocks.iron_directionalnocollision_1,1,0));//Axe
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_8,1,3));//Iron Hatchet
        items.add(new ItemStack(MetaBlocks.iron_directionalnocollision_1,1,1));//Sword
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_3,1,1));//Small Farming Tools
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_3,1,2));//Brooms
        items.add(new ItemStack(MetaBlocks.wood_directionalnocollision_2,1,0));//Broom
        items.add(new ItemStack(MetaBlocks.wood_directionalcollisiontrapdoor_3,1,3));//Farming Tools
        items.add(new ItemStack(MetaBlocks.wood_directionalnocollision_2,1,1));//Pitchfork
        items.add(new ItemStack(MetaBlocks.iron_directionalnocollision_1,1,2));//Shovel
        items.add(new ItemStack(MetaBlocks.wood_directionalnocollision_1,1,2));//Fishing Rod
        items.add(new ItemStack(MetaBlocks.wood_directionalnocollision_1,1,3));//Bow Saw
        items.add(new ItemStack(MetaBlocks.iron_directionalnocollision_1,1,3));//Hammer
        items.add(new ItemStack(MetaBlocks.iron_directionalnocollision_2,1,0));//Pickaxe
        items.add(new ItemStack(MetaBlocks.iron_directionalnocollision_2,1,1));//Sickle
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_8,1,1));//Blacksmith Tool Rack (Wall)
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_8,1,2));//Blacksmith Tool Rack
        items.add(new ItemStack(MetaBlocks.stone_directionalfullpartial_1,1,3));//Bellows
        items.add(new ItemStack(MetaBlocks.stone_directionalfullpartial_1,1,2));//Grindstone
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_9,1,1));//Tanning Rack
        items.add(new ItemStack(MetaBlocks.iron_trapdoormodel_2,1,0));//Circular Saw
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,7));//Straight Saw
        items.add(new ItemStack(MetaBlocks.ladder,1,0)); //Ladder
        items.add(new ItemStack(MetaBlocks.ladder_big,1,0)); //Big Ladder
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,0));//Pile of Paper
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,1));//Quill and Parchment
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,1));//Book
        items.add(new ItemStack(MetaBlocks.cloth_directionalnocollision_1,1,1));//Book
        items.add(new ItemStack(MetaBlocks.wheel,1,0));//Wheel
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,2));//Net
        items.add(new ItemStack(MetaBlocks.cloth_pane_1,1,1));//Fishing Net
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,3));//Net on the Ground
        items.add(new ItemStack(MetaBlocks.stone_fullpartial_6,1,12)); //Fishing Pot

        items.add(new ItemStack(MetaBlocks.stone_directionalnocollision_1,1,0));//Scales (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_directionalnocollision_1,1,1));//Abacus (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,1));//Hourglass (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_directionalnocollision_1,1,2));//Sextant (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_directionalnocollision_1,1,3));//Telescope (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_directionalnocollision_2,1,0));//Elven Telescope (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,2));//Astrolabe (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,1));//Elven Astrolabe (Web Model)
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,3));//Compass (Web Model)
        items.add(new ItemStack(Blocks.ANVIL,1,0)); //Anvil
        items.add(new ItemStack(MetaBlocks.anvil_plain,1,0)); //Anvil Plain
        items.add(new ItemStack(MetaBlocks.anvil_sword,1,0)); //Anvil Sword
        items.add(new ItemStack(Blocks.STICKY_PISTON,1,0)); //Sticky Piston
        items.add(new ItemStack(Blocks.PISTON,1,0)); //Piston
        items.add(new ItemStack(Blocks.TNT,1,0)); //TNT
        items.add(new ItemStack(Blocks.STONE_PRESSURE_PLATE,1,0)); //Stone Pressure Plate
        items.add(new ItemStack(Blocks.WOODEN_PRESSURE_PLATE,1,0)); //Wood Pressure Plate
        items.add(new ItemStack(Blocks.LIGHT_WEIGHTED_PRESSURE_PLATE,1,0)); //Light Pressure Plate
        items.add(new ItemStack(Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE,1,0)); //Heavy Pressure Plate
        items.add(new ItemStack(Blocks.DAYLIGHT_DETECTOR,1,0)); //Daylight Sensor
        items.add(new ItemStack(Items.REDSTONE,1,0)); //Redstone
        items.add(new ItemStack(Items.REPEATER,1,0)); //Repeater
        items.add(new ItemStack(Items.COMPARATOR,1,0)); //Comparator
        items.add(new ItemStack(Blocks.LEVER,1,0)); //Lever
        items.add(new ItemStack(MetaBlocks.lever,1,0));//Lever
        items.add(new ItemStack(Blocks.TRIPWIRE_HOOK,1,0)); //Tripwire Hook
        items.add(new ItemStack(MetaBlocks.hook,1,0));//Tripwire Hook
        CreativeTabs.TOOLS.displayAllRelevantItems(items);
        TRANSPORTATION.displayAllRelevantItems(items);
        COMBAT.displayAllRelevantItems(items);

    }
    
    /* Alternatively (or additionally), you could manually add itemstacks this way:
    
    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }
    
    */

    @Override
    public Item getTabIconItem() {
        return Items.IRON_AXE;
    }


}