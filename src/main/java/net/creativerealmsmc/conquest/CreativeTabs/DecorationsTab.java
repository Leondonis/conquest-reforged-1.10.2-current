package net.creativerealmsmc.conquest.CreativeTabs;

import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.creativerealmsmc.conquest.items.PaintingItem;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.List;

public class DecorationsTab extends CreativeTabs {

    public DecorationsTab(int index, String newLabel) {
        super(index, newLabel);
    }

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // Adds all the items that would normally be in the Vanilla Tab to the 'items' items
        //vanillaTab.displayAllRelevantItems(items);
        //  Storage  //
        items.add(new ItemStack(MetaBlocks.wood_fulltrapdoormodel_1,1,0));//3D Barrel
        items.add(new ItemStack(MetaBlocks.wood_log_3,1,3));//Barrel
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,8)); //Barrel of Dirt
        items.add(new ItemStack(MetaBlocks.barrel_grille_cauldron,1,0));//Empty Barrel with Grille
        items.add(new ItemStack(MetaBlocks.barrel_cauldron,1,0));//Empty Barrel
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,5));//Empty Bucket
        items.add(new ItemStack(MetaBlocks.wood_log_4,1,0));//Wicker Basket
        items.add(new ItemStack(MetaBlocks.wood_fullpartial_2,1,0));//Empty Basket
        items.add(new ItemStack(MetaBlocks.wood_fullpartial_2,1,1));//Empty Light Basket
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,11));//Barrel of Iron Ore
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,12));//Barrel of Diamond Ore
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,13));//Barrel of Emeralds
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,14));//Barrel of Lapis Lazuli
        items.add(new ItemStack(MetaBlocks.wood_full_5,1,15));//Barrel of Clay
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,0));//Gold Chest
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,1));
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,2));
        items.add(new ItemStack(MetaBlocks.stone_layer_8,1,0));//Gold Coin Pile Layer
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,3));//Sack of Rubies
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,4));//Sack of Pipeweed
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,5));//Barrel of Coal
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,10));//Charcoal
        items.add(new ItemStack(MetaBlocks.cauldron_irongrille,1,0));//Iron Cauldron with Grille
        items.add(new ItemStack(MetaBlocks.cauldron,1,0));//Cauldron
        items.add(new ItemStack(Items.CAULDRON,1,0));
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,6));//Crate
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,4));//Crate Tan Planks
        items.add(new ItemStack(MetaBlocks.wood_full_7,1,6));//Crate Gray Planks
        items.add(new ItemStack(MetaBlocks.wood_full_6,1,7));//Covered Crate


        //  Lights  //
        items.add(new ItemStack(MetaBlocks.campfire,1,0)); //Camp Fire
        items.add(new ItemStack(Blocks.TORCH,1,0)); //Torch
        items.add(new ItemStack(MetaBlocks.torch_10_1,1,0));//Elven Torch
        items.add(new ItemStack(MetaBlocks.torch_10_1,1,1));//Opaque Hanging Lantern
        items.add(new ItemStack(MetaBlocks.torch_10_2,1,0));//Hanging Lantern
        items.add(new ItemStack(Blocks.REDSTONE_TORCH,1,0)); //Redstone Torch
        items.add(new ItemStack(MetaBlocks.stone_nocollision_3,1,6)); //Unlit Candle

        items.add(new ItemStack(Blocks.REDSTONE_LAMP,1,0)); //Redstone Lamp
        items.add(new ItemStack(Blocks.GLOWSTONE,1,0)); //Glowstone
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,0));//Large Lamp
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,2));//Big Lantern
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,1));//Small Lantern
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,7));//Lit Chandelier
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,4));//Lantern and Lit Candle
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,5));//Lit Hand Candle
        items.add(new ItemStack(MetaBlocks.iron_fullpartiallight10_1,1,6));//Lit Candles
        items.add(new ItemStack(MetaBlocks.iron_brewingstandlight10_1,1,0));//Lit Candelabra
        items.add(new ItemStack(MetaBlocks.cloth_fulllight10_1,1,0));//White Paper Lantern
        items.add(new ItemStack(MetaBlocks.cloth_fulllight10_1,1,1));//Yellow Paper Lantern
        items.add(new ItemStack(MetaBlocks.cloth_nocollisionlight10_1,1,0));//Asian Lantern
        items.add(new ItemStack(Blocks.SEA_LANTERN,1,0)); //Sea Lantern
        items.add(new ItemStack(MetaBlocks.stone_fullpartial_6,1,11));//Elostirion Palantir


        //  Ropes and Chains  //
        items.add(new ItemStack(MetaBlocks.cloth_pane_1,1,0));//Wall of Ropes
        items.add(new ItemStack(MetaBlocks.cloth_pane_1,1,3));//Rope
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,9));//Rope 2
        items.add(new ItemStack(MetaBlocks.rope_rail,1,0));//Rope (Rail)
        items.add(new ItemStack(MetaBlocks.rope_climbing_ladder,1,0));//Climbing Rope
        items.add(new ItemStack(MetaBlocks.cloth_directionalnocollision_1,1,0));//Noose
        items.add(new ItemStack(MetaBlocks.rope_ladder,1,0));//Rope Ladder
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,11));//Iron Chains
        items.add(new ItemStack(MetaBlocks.ironchains_rail,1,0));//Iron Chains (Rail)
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,12));//Rusty Chains
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,13));//Small Chains
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,14));//Gold Chains
        items.add(new ItemStack(MetaBlocks.stone_nocollision_1,1,15));//Rope Hook
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,0));//Metal Hook
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,10));//Hanging Bags

        //  Bars and Panes  //
        items.add(new ItemStack(MetaBlocks.iron_trapdoormodel_1,1,1));//Cage Bars
        items.add(new ItemStack(Blocks.IRON_BARS,1,0)); //Iron Bars
        items.add(new ItemStack(MetaBlocks.glass_glass_2,1,4));//Iron Bars
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,1));//Dark Iron Fence
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,2));//Rusted Iron Fence
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,8)); //Iron Decorative Spikes
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,3));//Iron Fence
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,4));//Cell Bars
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,5));//Iron Grille
        items.add(new ItemStack(MetaBlocks.iron_ironbar_1,1,6));//Weather Vane
        items.add(new ItemStack(Blocks.END_ROD,1,0)); //End Rod



        //  Cloth  //
        items.add(new ItemStack(Blocks.WOOL,1,0)); //Wool
        items.add(new ItemStack(Blocks.WOOL,1,1)); //Orange Wool
        items.add(new ItemStack(Blocks.WOOL,1,2)); //Magenta Wool
        items.add(new ItemStack(Blocks.WOOL,1,3)); //Light Blue Wool
        items.add(new ItemStack(Blocks.WOOL,1,4)); //Yellow Wool
        items.add(new ItemStack(Blocks.WOOL,1,5)); //Lime Wool
        items.add(new ItemStack(Blocks.WOOL,1,6)); //Pink Wool
        items.add(new ItemStack(Blocks.WOOL,1,7)); //Gray Wool
        items.add(new ItemStack(Blocks.WOOL,1,8)); //Light Gray Wool
        items.add(new ItemStack(Blocks.WOOL,1,9)); //Cyan Wool
        items.add(new ItemStack(Blocks.WOOL,1,10)); //Purple Wool
        items.add(new ItemStack(Blocks.WOOL,1,11)); //Blue Wool
        items.add(new ItemStack(Blocks.WOOL,1,12)); //Brown Wool
        items.add(new ItemStack(Blocks.WOOL,1,13)); //Green Wool
        items.add(new ItemStack(Blocks.WOOL,1,14)); //Red Wool
        items.add(new ItemStack(Blocks.WOOL,1,15)); //Black Wool
        items.add(new ItemStack(Blocks.CARPET,1,0)); //White Carpet
        items.add(new ItemStack(Blocks.CARPET,1,1)); //Orange Carpet
        items.add(new ItemStack(Blocks.CARPET,1,2)); //Magenta Carpet
        items.add(new ItemStack(Blocks.CARPET,1,3)); //Light Blue Carpet
        items.add(new ItemStack(Blocks.CARPET,1,4)); //Yellow Carpet
        items.add(new ItemStack(Blocks.CARPET,1,5)); //Lime Carpet
        items.add(new ItemStack(Blocks.CARPET,1,6)); //Pink Carpet
        items.add(new ItemStack(Blocks.CARPET,1,7)); //Gray Carpet
        items.add(new ItemStack(Blocks.CARPET,1,8)); //Light Gray Carpet
        items.add(new ItemStack(Blocks.CARPET,1,9)); //Cyan Carpet
        items.add(new ItemStack(Blocks.CARPET,1,10)); //Purple Carpet
        items.add(new ItemStack(Blocks.CARPET,1,11)); //Blue Carpet
        items.add(new ItemStack(Blocks.CARPET,1,12)); //Brown Carpet
        items.add(new ItemStack(Blocks.CARPET,1,13)); //Green Carpet
        items.add(new ItemStack(Blocks.CARPET,1,14)); //Red Carpet
        items.add(new ItemStack(Blocks.CARPET,1,15)); //Black Carpet
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,0));//Black Curtain
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,6));//White Curtain
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,5));//Red Curtain
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,3));//Green Curtain
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,2));
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,1));//Blue Curtain
        items.add(new ItemStack(MetaBlocks.cloth_climbironbar_1,1,4));//Purple Curtain
        items.add(new ItemStack(Items.BANNER,1,15)); //Banner
        items.add(new ItemStack(Items.BANNER,1,14)); //Banner
        items.add(new ItemStack(Items.BANNER,1,13)); //Banner
        items.add(new ItemStack(Items.BANNER,1,12)); //Banner
        items.add(new ItemStack(Items.BANNER,1,11)); //Banner
        items.add(new ItemStack(Items.BANNER,1,10)); //Banner
        items.add(new ItemStack(Items.BANNER,1,9)); //Banner
        items.add(new ItemStack(Items.BANNER,1,8)); //Banner
        items.add(new ItemStack(Items.BANNER,1,7)); //Banner
        items.add(new ItemStack(Items.BANNER,1,6)); //Banner
        items.add(new ItemStack(Items.BANNER,1,5)); //Banner
        items.add(new ItemStack(Items.BANNER,1,4)); //Banner
        items.add(new ItemStack(Items.BANNER,1,3)); //Banner
        items.add(new ItemStack(Items.BANNER,1,2)); //Banner
        items.add(new ItemStack(Items.BANNER,1,1)); //Banner
        items.add(new ItemStack(Items.BANNER,1,0)); //Banner
        items.add(new ItemStack(MetaBlocks.cloth_ironclimbironbar_1,1,0));//Clothesline
        items.add(new ItemStack(MetaBlocks.cloth_ironclimbironbar_1,1,1));//Decorative Flags
        items.add(new ItemStack(MetaBlocks.cloth_ironclimbironbar_1,1,2));
        items.add(new ItemStack(MetaBlocks.cloth_pane_1,1,2));//Bear Hide
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,5));//Bear Skin
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,4));//Bear Skin 2
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,6));//Wolf Skin
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,7));//Sheep Skin
        items.add(new ItemStack(MetaBlocks.cloth_nocollision_1,1,8));//Hay on the Ground
        items.add(new ItemStack(MetaBlocks.plants_nocollision_7,1,6));//Scarecrow Head


        //  Misc  //
        items.add(new ItemStack(MetaBlocks.button_brass,1,0));//Brass Button
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,4));//Silver Coins
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,5));//Gold Coins
        items.add(new ItemStack(MetaBlocks.stone_fence_1,1,7));//Terracotta Grille
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,9));//Amphora
        items.add(new ItemStack(MetaBlocks.stone_full_18,1,13)); //Stone Urn
        items.add(new ItemStack(MetaBlocks.urn_terracotta_endframe,1,0));//Pot
        items.add(new ItemStack(Items.FLOWER_POT)); //Flower Pot
        items.add(new ItemStack(MetaBlocks.flowerpot,1,0));//Flower Pot
        items.add(new ItemStack(MetaBlocks.flowerpot_black,1,0));//Black Pot
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,6));//Pot with Water
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,7));//Cannonball
        items.add(new ItemStack(MetaBlocks.stone_nocollision_2,1,8));//Hell's Eye
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,0));//Checkerboard
        items.add(new ItemStack(MetaBlocks.cloth_directionalnocollision_1,1,2));//Chess Board
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,2));//Playing Cards
        items.add(new ItemStack(MetaBlocks.ground_directionalfullpartial_1,1,2));//Piñata
        items.add(new ItemStack(MetaBlocks.stone_fullpartial_6,1,1));//Gilded Bell
        items.add(new ItemStack(Blocks.DIAMOND_BLOCK,1,0)); //Diamond Block (Mirror)
        items.add(new ItemStack(MetaBlocks.glass_full_1,1,0));//Mirror
        items.add(new ItemStack(MetaBlocks.wood_hopperdirectional_10,1,2));//Mirror
        items.add(new ItemStack(Items.PAINTING)); //Painting

        for (PaintingItem painting : PaintingItem.getItems()) {
            items.add(new ItemStack(painting));
        }

        items.add(new ItemStack(Items.SIGN)); //Sign
        items.add(new ItemStack(MetaBlocks.wood_smallpillar_4, 1, 3)); //Sign Post
        items.add(new ItemStack(Items.ITEM_FRAME)); //Item Frame
        items.add(new ItemStack(Blocks.TRAPDOOR,1,0)); //Trapdoor
        items.add(new ItemStack(MetaBlocks.wood_nocollision_1,1,4)); //Elven Eisel









        // Sort the items by their UnLocalized name (or whatever)
/*
        Collections.sort(items, new Comparator<ItemStack>() {
            @Override
            public int compare(ItemStack i1, ItemStack i2) {
                return i1.getItem().getUnlocalizedName().compareTo(i2.getItem().getUnlocalizedName());
            }
        });
        */
    }

    /* Alternatively (or additionally), you could manually add itemstacks this way:

    @Override
    public void displayAllRelevantItems(List<ItemStack> items) {
        // new ItemStack(Item, Quantity, Damage)
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 0));
        items.add(new ItemStack(Item.forBlock(Block.WHATEVER), 1, 1));
        items.add(new ItemStack(Item.forBlock(Block.DOOT), 1, 0));
    }

    */

    @Override
    public Item getTabIconItem() {
        return Item.getItemFromBlock(Blocks.REDSTONE_TORCH);
    }


}