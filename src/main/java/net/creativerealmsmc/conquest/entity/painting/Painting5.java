package net.creativerealmsmc.conquest.entity.painting;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * @author dags <dags@dags.me>
 */
public class Painting5 extends PaintingBase {

    public static IPaintingSupplier CREATOR = new IPaintingSupplier() {
        @Override
        public PaintingBase createEntity(World worldIn, BlockPos pos, EnumFacing clickedSide, Art art) {
            return new Painting5(worldIn, pos, clickedSide, art);
        }
    };

    public Painting5(World worldIn) {
        super(worldIn);
    }

    public Painting5(World worldIn, BlockPos pos, EnumFacing facing, Art art) {
        super(worldIn, pos, facing, art);
    }
}
