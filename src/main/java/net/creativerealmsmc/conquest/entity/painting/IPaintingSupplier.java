package net.creativerealmsmc.conquest.entity.painting;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * @author dags <dags@dags.me>
 */
public interface IPaintingSupplier {

    PaintingBase createEntity(World worldIn, BlockPos pos, EnumFacing clickedSide, Art art);
}
