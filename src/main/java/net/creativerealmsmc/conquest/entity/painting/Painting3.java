package net.creativerealmsmc.conquest.entity.painting;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * @author dags <dags@dags.me>
 */
public class Painting3 extends PaintingBase {

    public static IPaintingSupplier CREATOR = new IPaintingSupplier() {
        @Override
        public PaintingBase createEntity(World worldIn, BlockPos pos, EnumFacing clickedSide, Art art) {
            return new Painting3(worldIn, pos, clickedSide, art);
        }
    };

    public Painting3(World worldIn) {
        super(worldIn);
    }

    public Painting3(World worldIn, BlockPos pos, EnumFacing facing, Art art) {
        super(worldIn, pos, facing, art);
    }
}
