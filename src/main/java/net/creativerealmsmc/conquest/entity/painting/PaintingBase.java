package net.creativerealmsmc.conquest.entity.painting;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityHanging;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * @author dags <dags@dags.me>
 */

// Base for all custom paintings
public abstract class PaintingBase extends EntityHanging implements IEntityAdditionalSpawnData {

    public Art art = Art.A1x1_0;

    public PaintingBase(World worldIn) {
        super(worldIn);
    }

    public PaintingBase(World worldIn, BlockPos pos, EnumFacing facing, Art art) {
        super(worldIn, pos);
        this.art = art;
        this.updateFacingWithBoundingBox(facing);
    }

    @Override
    public void onBroken(Entity brokenEntity) {
    }

    @Override
    public boolean onValidSurface() {
        return true;
    }

    @Override
    public int getWidthPixels() {
        return this.art.sizeX;
    }

    @Override
    public int getHeightPixels() {
        return this.art.sizeY;
    }

    @Override
    public void playPlaceSound() {
        this.playSound(SoundEvents.ENTITY_PAINTING_PLACE, 1.0F, 1.0F);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound tagCompound) {
        super.writeEntityToNBT(tagCompound);
        tagCompound.setInteger("ArtID", this.art.index());
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound tagCompound) {
        int id = tagCompound.getInteger("ArtID");
        this.art = Art.fromId(id);
        super.readEntityFromNBT(tagCompound);
    }

    @Override
    public void writeSpawnData(ByteBuf buffer) {
        buffer.writeInt(hangingPosition.getX());
        buffer.writeInt(hangingPosition.getY());
        buffer.writeInt(hangingPosition.getZ());
        buffer.writeInt(getHorizontalFacing().getIndex());
        buffer.writeInt(art.index());
    }

    @Override
    public void readSpawnData(ByteBuf additionalData) {
        int x = additionalData.readInt();
        int y = additionalData.readInt();
        int z = additionalData.readInt();

        int facing = additionalData.readInt();
        int id = additionalData.readInt();

        this.art = Art.fromId(id);
        this.setPosition(x, y, z);
        this.updateFacingWithBoundingBox(EnumFacing.getFront(facing));
    }

    @SideOnly(Side.CLIENT)
    public void setPositionAndRotationDirect(double x, double y, double z, float a, float b, int c, boolean d) {
        BlockPos pos = this.hangingPosition.add(x - this.posX, y - this.posY, z - this.posZ);
        this.setPosition((double) pos.getX(), (double) pos.getY(), (double) pos.getZ());
    }
}
