package net.creativerealmsmc.conquest.proxy;

import net.creativerealmsmc.conquest.Main;
import net.creativerealmsmc.conquest.init.Entities;
import net.creativerealmsmc.conquest.init.Items;
import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.creativerealmsmc.conquest.init.ModColourManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.client.model.obj.OBJLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.io.IOException;

public class ClientProxy extends CommonProxy
{
    private final KeyBinding gamefolder = new KeyBinding("key.conquest.button.gamefolder", Keyboard.KEY_BACKSLASH, "Conquest Reforged");

	@Override
	public void preInit(FMLPreInitializationEvent e)
	{
		super.preInit(e);
		MetaBlocks.registerRenders();
		Entities.registerRenderers();
        ClientRegistry.registerKeyBinding(gamefolder);
        MinecraftForge.EVENT_BUS.register(this);
		OBJLoader.INSTANCE.addDomain(Main.MODID);
	}
	
	@Override
	public void init(FMLInitializationEvent e)
	{
		super.init(e);
		ModColourManager.registerColourHandlers();
		MetaBlocks.registerRendersNoMeta();
		Items.registerRenders();
	}

	@Override
	public void postInit(FMLPostInitializationEvent e) {
		super.postInit(e);
	}

	@Override
	public boolean playerIsInCreativeMode(EntityPlayer player)
	{
		if (player instanceof EntityPlayerMP)
	    {
			EntityPlayerMP entityPlayerMP = (EntityPlayerMP)player;
			return entityPlayerMP.interactionManager.isCreative();
	    }
		else if (player instanceof EntityPlayerSP)
	    {
			return Minecraft.getMinecraft().playerController.isInCreativeMode();
	    }
	    return false;
	}

	@Override
	public boolean isDedicatedServer()
	{
		return false;
	}

	@SubscribeEvent
	public void onTick(TickEvent.ClientTickEvent event) {
	    if (gamefolder.isPressed()) {
            try {
            	if (Minecraft.getMinecraft().inGameHasFocus) {
					Minecraft.getMinecraft().setIngameNotInFocus();
				}

                Desktop.getDesktop().open(Minecraft.getMinecraft().mcDataDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}