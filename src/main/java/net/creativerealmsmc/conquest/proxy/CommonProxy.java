package net.creativerealmsmc.conquest.proxy;

import net.creativerealmsmc.conquest.command.PaintingCommand;
import net.creativerealmsmc.conquest.init.Entities;
import net.creativerealmsmc.conquest.init.Items;
import net.creativerealmsmc.conquest.init.MetaBlocks;
import net.creativerealmsmc.conquest.init.Tabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

public abstract class CommonProxy
{
	public void preInit(FMLPreInitializationEvent e)
	{
		Tabs.init();

		Items.init();
		Items.register();

		MetaBlocks.init();
		MetaBlocks.register();

		Entities.register();
	}

	public void init(FMLInitializationEvent e)
	{

	}

	public void postInit(FMLPostInitializationEvent e)
	{

	}

	public void serverStart(FMLServerStartingEvent e)
	{
		e.registerServerCommand(new PaintingCommand());
	}

	abstract public boolean playerIsInCreativeMode(EntityPlayer player);
	abstract public boolean isDedicatedServer();
}
