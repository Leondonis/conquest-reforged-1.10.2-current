package net.creativerealmsmc.conquest.command;

import net.creativerealmsmc.conquest.entity.painting.Art;
import net.creativerealmsmc.conquest.items.PaintingItem;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author dags <dags@dags.me>
 */
public class PaintingCommand implements ICommand {

    @Override
    public String getCommandName() {
        return "paint";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/paint <painting_id> <art_id>";
    }

    @Override
    public List<String> getCommandAliases() {
        return Collections.singletonList("paint");
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (!(sender instanceof EntityPlayerMP)) {
            return;
        }

        if (args.length > 0) {
            String input = StringUtils.join(args, " ");
            ItemStack stack = null;

            outer:
            for (String id : PaintingItem.getPaintingIds()) {
                PaintingItem item = PaintingItem.getPainting(id);

                for (Art art : Art.values()) {
                    ItemStack test = new ItemStack(item, 1, art.index());

                    String name0 = item.getItemStackDisplayName(test);
                    if (name0.equalsIgnoreCase(input)) {
                        stack = test;
                        break outer;
                    }

                    String name1 = id + " " + art.shapeId;
                    if (name1.equalsIgnoreCase(input)) {
                        stack = test;
                        break outer;
                    }
                }
            }

            if (stack != null) {
                EntityPlayerMP player = (EntityPlayerMP) sender;
                player.inventory.addItemStackToInventory(stack);
                return;
            }

            throw new CommandException(String.format("Invalid painting: '%s'", input));
        }

        throw new CommandException("Not enough arguments!");
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return sender.canCommandSenderUseCommand(0, getCommandName());
    }

    @Override
    public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
        if (args.length > 0) {
            Set<String> results = new LinkedHashSet<String>();

            String first = args[0].toLowerCase();
            String joined = StringUtils.join(args, " ").toLowerCase();

            addMatches(first, PaintingItem.getPaintingIds().toArray(), results, true);
            addMatches(joined, PaintingItem.getLocalizedNames().toArray(), results, false);

            if (args.length > 1) {
                addMatches(args[args.length - 1].toLowerCase(), Art.values(), results, false);
            }

            List<String> list = new ArrayList<String>(results);
            Collections.sort(list);
            return list;
        }
        return Collections.emptyList();
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return getCommandName().compareTo(o.getCommandName());
    }

    private static void addMatches(String input, Object[] array, Collection<String> results, boolean allowEmpty) {
        for (Object o : array) {
            if (o.toString().toLowerCase().startsWith(input)) {
                results.add(o.toString());
            }
        }
    }
}
