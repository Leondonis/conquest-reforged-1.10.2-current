package net.creativerealmsmc.conquest.init;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.BlockColors;
import net.minecraft.client.renderer.color.IBlockColor;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.client.renderer.color.ItemColors;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ColorizerGrass;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.biome.BiomeColorHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Registers {@link IBlockColor}/{@link IItemColor} handlers for this mod's blocks/items.
 *
 * @author Choonster
 */
@SideOnly(Side.CLIENT)
public class ModColourManager {
	private static final Minecraft minecraft = Minecraft.getMinecraft();

	/**
	 * Register the colour handlers.
	 */
	public static void registerColourHandlers() {
		final BlockColors blockColors = minecraft.getBlockColors();
		final ItemColors itemColors = minecraft.getItemColors();

		registerBlockColourHandlers(blockColors);
		registerItemColourHandlers(blockColors, itemColors);
	}

	/**
	 * Register the {@link IBlockColor} handlers.
	 *
	 * @param blockColors The BlockColors instance
	 */
	private static void registerBlockColourHandlers(final BlockColors blockColors) {
		// Use the grass colour of the biome or the default grass colour
		final IBlockColor grassColourHandler = new IBlockColor() {
			@Override
			public int colorMultiplier(IBlockState state, IBlockAccess blockAccess, BlockPos pos, int tintIndex) {
				if (blockAccess != null && pos != null) {
					return BiomeColorHelper.getGrassColorAtPos(blockAccess, pos);
				}

				return ColorizerGrass.getGrassColor(0.5D, 1.0D);
			}
		};

		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.plants_nocollisionbiome_1);
		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.vine_jungle);
		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.vine_ivy);
		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.vine_moss);
		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.grass_layer_1);
		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.leaves_fullbiome_1);
		blockColors.registerBlockColorHandler(grassColourHandler, MetaBlocks.grass_full_1);
	}

	/**
	 * Register the {@link IItemColor} handlers
	 *
	 * @param blockColors The BlockColors instance
	 * @param itemColors  The ItemColors instance
	 */
	private static void registerItemColourHandlers(final BlockColors blockColors, final ItemColors itemColors) {
		// Use the Block's colour handler for an ItemBlock
		final IItemColor itemBlockColourHandler = new IItemColor() {
			@Override
			public int getColorFromItemstack(ItemStack stack, int tintIndex) {
				IBlockState iblockstate = ((ItemBlock) stack.getItem()).getBlock().getStateFromMeta(stack.getMetadata());
				return blockColors.colorMultiplier(iblockstate, null, null, tintIndex);
			}
		};

		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.plants_nocollisionbiome_1);
		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.vine_jungle);
		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.vine_ivy);
		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.vine_moss);
		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.grass_layer_1);
		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.leaves_fullbiome_1);
		itemColors.registerItemColorHandler(itemBlockColourHandler, MetaBlocks.grass_full_1);

	}
}