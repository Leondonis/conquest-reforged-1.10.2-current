package net.creativerealmsmc.conquest.init;

import net.creativerealmsmc.conquest.Main;
import net.creativerealmsmc.conquest.entity.painting.*;
import net.creativerealmsmc.conquest.renderer.entity.PaintingRender;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;

/**
 * @author dags <dags@dags.me>
 */
public class Entities {

    public static void init() {

    }

    public static void register() {
        IDGenerator entityID = new IDGenerator();
        registerPaintingEntity(Painting0.class, "painting0", entityID.next());
        registerPaintingEntity(Painting1.class, "painting1", entityID.next());
        registerPaintingEntity(Painting2.class, "painting2", entityID.next());
        registerPaintingEntity(Painting3.class, "painting3", entityID.next());
        registerPaintingEntity(Painting4.class, "painting4", entityID.next());
        registerPaintingEntity(Painting5.class, "painting5", entityID.next());
        registerPaintingEntity(Painting6.class, "painting6", entityID.next());
        registerPaintingEntity(Painting7.class, "painting7", entityID.next());
        registerPaintingEntity(Painting8.class, "painting8", entityID.next());
        registerPaintingEntity(Painting9.class, "painting9", entityID.next());
    }

    public static void registerRenderers() {
        registerPaintingRenderer(Painting0.class, "painting0");
        registerPaintingRenderer(Painting1.class, "painting1");
        registerPaintingRenderer(Painting2.class, "painting2");
        registerPaintingRenderer(Painting3.class, "painting3");
        registerPaintingRenderer(Painting4.class, "painting4");
        registerPaintingRenderer(Painting5.class, "painting5");
        registerPaintingRenderer(Painting6.class, "painting6");
        registerPaintingRenderer(Painting7.class, "painting7");
        registerPaintingRenderer(Painting8.class, "painting8");
        registerPaintingRenderer(Painting9.class, "painting9");
    }

    private static void registerPaintingEntity(Class<? extends PaintingBase> entityClass, String name, int id) {
        Object plugin = Main.instance;
        int trackingRange = 128;
        int updateFrequency = 1;
        boolean sendsVelocity = false;
        EntityRegistry.registerModEntity(entityClass, name, id, plugin, trackingRange, updateFrequency, sendsVelocity);
    }

    private static void registerPaintingRenderer(Class<? extends PaintingBase> entityClass, String name) {
        PaintingRender.Factory factory = new PaintingRender.Factory(name);
        RenderingRegistry.registerEntityRenderingHandler(entityClass, factory);
    }

    private static class IDGenerator {

        private int id = 0;

        private int next() {
            return id++;
        }
    }
}
