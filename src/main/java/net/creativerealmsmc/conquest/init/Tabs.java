package net.creativerealmsmc.conquest.init;

import net.creativerealmsmc.conquest.CreativeTabs.*;
import net.minecraft.creativetab.CreativeTabs;

import static net.minecraft.creativetab.CreativeTabs.*;

public class Tabs {

    public static final CreativeTabs refined_simple = new SimpleRefinedTab(getTabIndex(BUILDING_BLOCKS), "refined_simple");
    public static final CreativeTabs refined_advanced = new AdvancedRefinedTab(getTabIndex(DECORATIONS), "refined_advanced");
    public static final CreativeTabs decorations = new DecorationsTab(getTabIndex(REDSTONE), "decorations");
    public static final CreativeTabs furniture = new FurnitureTab(getTabIndex(TRANSPORTATION), "furniture");
    public static final CreativeTabs topography = new TopographyTab(getTabIndex(COMBAT), "topography");
    public static final CreativeTabs natural = new NaturalTab(getTabIndex(BREWING), "natural");
    public static final CreativeTabs food = new FoodTab(getTabIndex(FOOD), "food");
    public static final CreativeTabs tools = new ToolsTab(getTabIndex(TOOLS), "tools");
    public static final CreativeTabs misc = new MiscellaneousTab(getTabIndex(MISC), "misc");
    public static final CreativeTabs materials = new MaterialsTab(getTabIndex(MATERIALS), "materials");

    public static void init() {
        // nothing required here, just need to make sure the above fields actually get instantiated
    }

    // Required because CreativeTabs.getTabIndex() is client-only so will cause a crash if called on the server
    private static int getTabIndex(net.minecraft.creativetab.CreativeTabs tab) {
        for (int i = 0; i < CreativeTabs.CREATIVE_TAB_ARRAY.length; i++) {
            if (CreativeTabs.CREATIVE_TAB_ARRAY[i] == tab) {
                return i;
            }
        }
        // no match found so return the end of the array as the index
        return CreativeTabs.CREATIVE_TAB_ARRAY.length;
    }
}
