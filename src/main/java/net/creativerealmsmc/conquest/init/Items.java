package net.creativerealmsmc.conquest.init;

import net.creativerealmsmc.conquest.Main;
import net.creativerealmsmc.conquest.entity.painting.*;
import net.creativerealmsmc.conquest.items.ItemShield;
import net.creativerealmsmc.conquest.items.PaintingItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Items {

    public static PaintingItem paint0;
    public static PaintingItem paint1;
    public static PaintingItem paint2;
    public static PaintingItem paint3;
    public static PaintingItem paint4;
    public static PaintingItem paint5;
    public static PaintingItem paint6;
    public static PaintingItem paint7;
    public static PaintingItem paint8;
    public static PaintingItem paint9;
    public static ItemShield shield_nordic;

    //public static Item rope;

    public static void init() {
        paint0 = new PaintingItem(Painting0.class, Painting0.CREATOR, Main.MODID, "painting0");
        paint1 = new PaintingItem(Painting1.class, Painting1.CREATOR, Main.MODID, "painting1");
        paint2 = new PaintingItem(Painting2.class, Painting2.CREATOR, Main.MODID, "painting2");
        paint3 = new PaintingItem(Painting3.class, Painting3.CREATOR, Main.MODID, "painting3");
        paint4 = new PaintingItem(Painting4.class, Painting4.CREATOR, Main.MODID, "painting4");
        paint5 = new PaintingItem(Painting5.class, Painting5.CREATOR, Main.MODID, "painting5");
        paint6 = new PaintingItem(Painting6.class, Painting6.CREATOR, Main.MODID, "painting6");
        paint7 = new PaintingItem(Painting7.class, Painting7.CREATOR, Main.MODID, "painting7");
        paint8 = new PaintingItem(Painting8.class, Painting8.CREATOR, Main.MODID, "painting8");
        paint9 = new PaintingItem(Painting9.class, Painting9.CREATOR, Main.MODID, "painting9");
        shield_nordic = new ItemShield(CreativeTabs.SEARCH, Main.MODID, "shield_nordic");
        // rope = new Rope("rope",CreativeTabs.MATERIALS);
    }

    public static void register() {
        GameRegistry.register(paint0);
        GameRegistry.register(paint1);
        GameRegistry.register(paint2);
        GameRegistry.register(paint3);
        GameRegistry.register(paint4);
        GameRegistry.register(paint5);
        GameRegistry.register(paint6);
        GameRegistry.register(paint7);
        GameRegistry.register(paint8);
        GameRegistry.register(paint9);
        GameRegistry.register(shield_nordic);

        // GameRegistry.register(rope);
    }

    public static void registerRenders() {
        registerPaintingItems(paint0);
        registerPaintingItems(paint1);
        registerPaintingItems(paint2);
        registerPaintingItems(paint3);
        registerPaintingItems(paint4);
        registerPaintingItems(paint5);
        registerPaintingItems(paint6);
        registerPaintingItems(paint7);
        registerPaintingItems(paint8);
        registerPaintingItems(paint9);
        registerShieldItems(shield_nordic);
    }

    private static void registerPaintingItems(PaintingItem item) {
        // Register item models for every Art variant
        String location = Main.MODID + ":" + item.getName();
        ModelResourceLocation modelLocation = new ModelResourceLocation(location, "inventory");
        for (Art art : Art.values()) {
            Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, art.index(), modelLocation);
        }
    }

    private static void registerShieldItems(ItemShield item) {
        // Register item models for every Art variant
        String location = Main.MODID + ":" + item.getName();
        ModelResourceLocation modelLocation = new ModelResourceLocation(location, "inventory");
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, modelLocation);
    }
}