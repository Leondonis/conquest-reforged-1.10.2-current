# README #

Welcome to the Conquest Reforged 1.10.2 repo!

### What is this repository for? ###

This repository tracks changes and updates to the 1.10.2 version of the Conquest Mod.
It is intended for Mod Developers and anyone else who wants to have the cutting edge
versions of the mod.

### How do I get set up? ###

This repository is not meant to be a complete work-space for the mod. This repository only tracks the source code and assets for the mod
and not the build scripts and folders. As such, the following instructions will help you set up a work-space for the mod
and then how to put a clone of this repository into that work-space.

You need to install the latest [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) before you follow the tutorial, otherwise it will not work. Make sure that you install the x86/64-bit version that matches your OS. If you don't this will lead to a java GC Overhead exception to the thrown during the :decompileMC: phase of the setup.

Set up a work-space for your desired IDE using the tutorial on [this](http://www.minecraftforge.net/wiki/Installation/Source) site.

Once the work-space is set up you need the actual source code for the mod. There are two ways of getting the code from Bitbucket onto your computer.

First, you can use the command line tool [GIT](https://git-scm.com) to clone the repository into your src folder. There is a tutorial for cloning a repository from Bitbucket available at [this](https://confluence.atlassian.com/bitbucket/create-and-clone-a-repository-800695642.html#Createandclonearepository-CloningaGitrepository) site. Once the repository has been cloned, delete the existing
"main" folder and rename the folder GIT created to "main".

Alternatively, you can use a GUI tool [Source Tree](https://www.sourcetreeapp.com). There is a tutorial for cloning a repository from Bitbucket available at [this](https://confluence.atlassian.com/bitbucket/create-and-clone-a-repository-800695642.html#Createandclonearepository-CloningarepositorywithSourceTree) site. If you choose to use Source Tree you must first empty the "main" folder inside of src and select it as the destination for the clone.

### Contribution guidelines ###
* Write comments! Although you might understand your code, others might not. This makes bug fixing much easier.
* When you make a commit, you will have to add a message with that commit. Avoid useless messages like "More Changes!".
  It's better to make smaller changes with specific commit messages than trying to lump them into one monolithic commit.


### Who do I talk to? ###

If you have questions about how the mod works or if you have problems setting up the project, talk to Origines on Discord.